/***************************************************************************
 *
 * gnome-mount-properties-view.h : Nautilus properties page
 *
 * Copyright (C) 2006 David Zeuthen, <david@fubar.dk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 **************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <libhal.h>
#include <libhal-storage.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <gconf/gconf-client.h>

#ifdef __FreeBSD__
#include <fstab.h>
#include <sys/param.h>
#include <sys/ucred.h>
#include <sys/mount.h>
#include <limits.h>
#include <pwd.h>
#else
#include <mntent.h>
#endif

#include "gnome-mount-properties-view.h"

struct _GnomeMountPropertiesView {
	GtkVBox base_instance;

	GladeXML *xml;
	char *keydir;
};

struct _GnomeMountPropertiesViewClass {
	GtkVBoxClass base_class;
};

#ifdef __FreeBSD__
static struct opt {
	int o_opt;
	const char *o_name;
} optnames[] = {
	{ MNT_ASYNC,		"asynchronous" },
	{ MNT_EXPORTED,		"NFS exported" },
	{ MNT_LOCAL,		"local" },
	{ MNT_NOATIME,		"noatime" },
	{ MNT_NOEXEC,		"noexec" },
	{ MNT_NOSUID,		"nosuid" },
	{ MNT_NOSYMFOLLOW,	"nosymfollow" },
	{ MNT_QUOTA,		"with quotas" },
	{ MNT_RDONLY,		"read-only" },
	{ MNT_SYNCHRONOUS,	"synchronous" },
	{ MNT_UNION,		"union" },
	{ MNT_NOCLUSTERR,	"noclusterr" },
	{ MNT_NOCLUSTERW,	"noclusterw" },
	{ MNT_SUIDDIR,		"suiddir" },
	{ MNT_SOFTDEP,		"soft-updates" },
	{ MNT_MULTILABEL,	"multilabel" },
	{ MNT_ACLS,		"acls" },
#ifdef MNT_GJOURNAL
	{ MNT_GJOURNAL,		"gjournal" },
#endif
	{ 0, NULL }
};
#endif

G_DEFINE_TYPE (GnomeMountPropertiesView, gm_properties_view, GTK_TYPE_VBOX)

static void
gm_properties_view_finalize (GObject *object)
{
	GnomeMountPropertiesView *properties = GM_PROPERTIES_VIEW (object);

	if (properties->xml != NULL) {
		g_object_unref (properties->xml);
		properties->xml = NULL;
	}

	if (properties->keydir != NULL) {
		g_free (properties->keydir);
		properties->keydir = NULL;
	}

        G_OBJECT_CLASS (gm_properties_view_parent_class)->finalize (object);
}

static void
gm_properties_view_class_init (GnomeMountPropertiesViewClass *properties_class)
{
	GObjectClass *g_object_class = G_OBJECT_CLASS (properties_class);

	g_object_class->finalize = gm_properties_view_finalize;
}

static char *
get_keydir (LibHalDrive *drive, LibHalVolume *volume)
{
	char *keydir;

	keydir = NULL;

	if (volume != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_volume_get_udi (volume));
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/volumes/%s", udi2);
		g_free (udi2);
	} else if (drive != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_drive_get_udi (drive));
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/drives/%s", udi2);
		g_free (udi2);
	} 

	return keydir;
}


static void
mount_point_entry_changed (GtkEditable *editable, gpointer user_data)
{
	GnomeMountPropertiesView *properties = GM_PROPERTIES_VIEW (user_data);
	char *text;
	char *key;
	GConfClient *gconf_client;

	if (properties->keydir == NULL) {
		g_warning ("properties->keydir not supposed to be NULL");
		return;
	}

	gconf_client = gconf_client_get_default ();

	key = g_strdup_printf ("%s/mount_point", properties->keydir);
	text = gtk_editable_get_chars (editable, 0, -1);
	if (text == NULL || strlen (text) == 0) {
		gconf_client_unset (gconf_client, key, NULL);
	} else if (text != NULL && strlen (text) > 0) {
		gconf_client_set_string (gconf_client, key, text, NULL);
	}
	g_free (text);
	g_free (key);

	g_object_unref (gconf_client);
}

static void
fstype_entry_changed (GtkEditable *editable, gpointer user_data)
{
	GnomeMountPropertiesView *properties = GM_PROPERTIES_VIEW (user_data);
	char *text;
	char *key;
	GConfClient *gconf_client;

	if (properties->keydir == NULL) {
		g_warning ("properties->keydir not supposed to be NULL");
		return;
	}

	gconf_client = gconf_client_get_default ();

	key = g_strdup_printf ("%s/fstype_override", properties->keydir);
	text = gtk_editable_get_chars (editable, 0, -1);
	if (text == NULL || strlen (text) == 0) {
		gconf_client_unset (gconf_client, key, NULL);
	} else if (text != NULL && strlen (text) > 0) {
		gconf_client_set_string (gconf_client, key, text, NULL);
	}
	g_free (text);
	g_free (key);

	g_object_unref (gconf_client);
}

static void
mount_options_entry_changed (GtkEditable *editable, gpointer user_data)
{
	GnomeMountPropertiesView *properties = GM_PROPERTIES_VIEW (user_data);
	char *text;
	char *key;
	GConfClient *gconf_client;

	if (properties->keydir == NULL) {
		g_warning ("properties->keydir not supposed to be NULL");
		return;
	}

	gconf_client = gconf_client_get_default ();

	key = g_strdup_printf ("%s/mount_options", properties->keydir);
	text = gtk_editable_get_chars (editable, 0, -1);
	if (text == NULL || strlen (text) == 0) {
		gconf_client_unset (gconf_client, key, NULL);
	} else if (text != NULL && strlen (text) > 0) {
		char **opts;
		GSList *l;
		int i;

		l = NULL;
		opts = g_strsplit (text, " ", 0);
		for (i = 0; opts[i] != NULL; i++) {
			l = g_slist_append (l, opts[i]);
		}
		
		gconf_client_set_list (gconf_client, key, GCONF_VALUE_STRING, l, NULL);
		
		g_strfreev (opts);
		g_slist_free (l);
	}
	g_free (text);
	g_free (key);

	g_object_unref (gconf_client);
}

static void
populate_ui_from_gconf (GnomeMountPropertiesView *properties,
			char                     *mount_point_widget,
			char                     *mount_options_widget,
			char                     *fstype_widget,
			char                     *expander_widget)
{
	GSList *options_list;
	GSList *l;
	GSList *next;
	char *key;
	char *mount_point;
	char *mount_options;
	char *fstype_override;
	GString *options;
	GConfClient *gconf_client;
	GladeXML *xml;

	if (properties->xml == NULL) {
		g_warning ("properties->xml not supposed to be NULL");
		return;
	}

	if (properties->keydir == NULL) {
		g_warning ("properties->keydir not supposed to be NULL");
		return;
	}

	g_debug ("Reading from gconf directory %s", properties->keydir);

	gconf_client = gconf_client_get_default ();

	key = g_strdup_printf ("%s/mount_point", properties->keydir);
	mount_point = gconf_client_get_string (gconf_client, key, NULL);
	g_free (key);
	
	options = g_string_new ("");
	key = g_strdup_printf ("%s/mount_options", properties->keydir);
	if ((options_list = gconf_client_get_list (gconf_client, key, GCONF_VALUE_STRING, NULL)) != NULL) {
		for (l = options_list; l != NULL; l = next) {
			char *option;
			
			next = l->next;
			option = l->data;
			
			if (options->len > 0)
				options = g_string_append_c (options, ' ');
			
			options = g_string_append (options, option);
			
			g_slist_free_1 (l);
		}
	}
	g_free (key);
	if (options->len == 0) {
		mount_options = NULL;
		g_string_free (options, TRUE);
	} else {
		mount_options = g_string_free (options, FALSE);
	}
	
	key = g_strdup_printf ("%s/fstype_override", properties->keydir);
	fstype_override = gconf_client_get_string (gconf_client, key, NULL);
	g_free (key);
	
	g_debug ("mount_point = '%s'", mount_point);
	g_debug ("mount_options = '%s'", mount_options);
	g_debug ("fstype_override = '%s'", fstype_override);
	
	if (mount_point != NULL) {
		gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (properties->xml, mount_point_widget)), mount_point);
		gtk_expander_set_expanded (GTK_EXPANDER (glade_xml_get_widget (properties->xml, expander_widget)), TRUE);
	}
	if (mount_options != NULL) {
		gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (properties->xml, mount_options_widget)), mount_options);
		gtk_expander_set_expanded (GTK_EXPANDER (glade_xml_get_widget (properties->xml, expander_widget)), TRUE);
	}
	if (fstype_override != NULL) {
		gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (properties->xml, fstype_widget)), fstype_override);
		gtk_expander_set_expanded (GTK_EXPANDER (glade_xml_get_widget (properties->xml, expander_widget)), TRUE);
	}
	
	g_free (mount_point);
	g_free (mount_options);
	g_free (fstype_override);
	
	g_object_unref (gconf_client);
}

#ifdef __FreeBSD__
struct mtab_handle
{
  struct statfs	*mounts;
  int		n_mounts;
  int		iter;
};
#endif

/* borrowed from gtk/gtkfilesystemunix.c in GTK+ on 02/23/2006 */
static void
canonicalize_filename (gchar *filename)
{
	gchar *p, *q;
	gboolean last_was_slash = FALSE;
	
	p = filename;
	q = filename;
	
	while (*p)
	{
		if (*p == G_DIR_SEPARATOR)
		{
			if (!last_was_slash)
				*q++ = G_DIR_SEPARATOR;
			
			last_was_slash = TRUE;
		}
		else
		{
			if (last_was_slash && *p == '.')
			{
				if (*(p + 1) == G_DIR_SEPARATOR ||
				    *(p + 1) == '\0')
				{
					if (*(p + 1) == '\0')
						break;
					
					p += 1;
				}
				else if (*(p + 1) == '.' &&
					 (*(p + 2) == G_DIR_SEPARATOR ||
					  *(p + 2) == '\0'))
				{
					if (q > filename + 1)
					{
						q--;
						while (q > filename + 1 &&
						       *(q - 1) != G_DIR_SEPARATOR)
							q--;
					}
					
					if (*(p + 2) == '\0')
						break;
					
					p += 2;
				}
				else
				{
					*q++ = *p;
					last_was_slash = FALSE;
				}
			}
			else
			{
				*q++ = *p;
				last_was_slash = FALSE;
			}
		}
		
		p++;
	}
	
	if (q > filename + 1 && *(q - 1) == G_DIR_SEPARATOR)
		q--;
	
	*q = '\0';
}

static char *
resolve_symlink (const char *file)
{
	GError *error;
	char *dir;
	char *link;
	char *f;
	char *f1;

	f = g_strdup (file);

	while (g_file_test (f, G_FILE_TEST_IS_SYMLINK)) {
		link = g_file_read_link (f, &error);
		if (link == NULL) {
			g_warning ("Cannot resolve symlink %s: %s", f, error->message);
			g_error_free (error);
			g_free (f);
			f = NULL;
			goto out;
		}
		
		dir = g_path_get_dirname (f);
		f1 = g_strdup_printf ("%s/%s", dir, link);
		g_free (dir);
		g_free (link);
		g_free (f);
		f = f1;
	}

out:
	if (f != NULL)
		canonicalize_filename (f);
	return f;
}

static gboolean
mtab_open (gpointer *handle)
{
#ifdef __FreeBSD__
	struct mtab_handle *mtab;

	mtab = g_new0 (struct mtab_handle, 1);
	mtab->n_mounts = getmntinfo (&mtab->mounts, MNT_NOWAIT);
	if (mtab->n_mounts == 0) {
		g_free (mtab);
		return FALSE;
	}

	*handle = mtab;
	return TRUE;
#else
	*handle = fopen ("/proc/mounts", "r");
	return *handle != NULL;
#endif
}

static gboolean
mtab_next (gpointer handle, char **device_file, char **mount_options, char **mount_fstype)
{
#ifdef __FreeBSD__
	struct mtab_handle *mtab = handle;

	if (mtab->iter < mtab->n_mounts) {
		struct opt *o;
		int flags;
		GString *optstr;

		optstr = g_string_new("");
		flags = mtab->mounts[mtab->iter].f_flags & MNT_VISFLAGMASK;

		for (o = optnames; flags && o->o_opt; o++) {
			if (flags & o->o_opt) {
				g_string_append_printf(optstr, ", %s", o->o_name);
				flags &= ~o->o_opt;
			}
		}
		*device_file = mtab->mounts[mtab->iter].f_mntfromname;
		*mount_options = g_string_free(optstr, FALSE);
		*mount_fstype = mtab->mounts[mtab->iter++].f_fstypename;
		return TRUE;
	} else {
		return FALSE;
	}
#else
	struct mntent *mnt;

	mnt = getmntent (handle);
	if (mnt != NULL) {
		*device_file = mnt->mnt_fsname;
		*mount_options = mnt->mnt_opts;
		*mount_fstype = mnt->mnt_type;
		return TRUE;
	} else {
		return FALSE;
	}
#endif
}

static void
mtab_close (gpointer handle)
{
#ifdef __FreeBSD__
	g_free (handle);
#else
	fclose (handle);
#endif
}




void
gm_properties_view_set_info_volume (GnomeMountPropertiesView *properties, 
				    LibHalVolume             *vol,
				    LibHalContext            *hal_ctx)
{
	GtkWidget *widget;
	char *fssize = NULL;
	char *fstype = NULL;
	char *fsuuid = NULL;
	char *fslabel = NULL;
	const char *drive_udi;
	LibHalDrive *drv;
	const char *media;
	const char *mount_point;

	drive_udi = libhal_volume_get_storage_device_udi (vol);
	drv = libhal_drive_from_udi (hal_ctx, drive_udi);

	/* Create a new GladeXML object from XML file */
	properties->xml = glade_xml_new (GLADEDIR "/gnome-mount-properties.glade", "gm_page_volume_root", GETTEXT_PACKAGE);
	g_assert (properties->xml != NULL);

	gtk_box_pack_start (GTK_BOX (properties),
			    glade_xml_get_widget (properties->xml, "gm_page_volume_root"),
			    TRUE, TRUE, 0);

	fssize = g_format_size_for_display (libhal_volume_get_size (vol));
	if (libhal_volume_get_fstype (vol) != NULL && libhal_volume_get_fsversion (vol) != NULL) {
		fstype = g_strdup_printf ("%s (%s)", 
					  libhal_volume_get_fstype (vol),
					  libhal_volume_get_fsversion (vol));
	} else if (libhal_volume_get_fstype (vol) != NULL) {
		fstype = g_strdup (libhal_volume_get_fstype (vol));
	}
	fsuuid = g_strdup (libhal_volume_get_uuid (vol));
	fslabel = g_strdup (libhal_volume_get_label (vol));

	media = "";
	if (drv != NULL) {
		switch (libhal_drive_get_type (drv)) {
		case LIBHAL_DRIVE_TYPE_REMOVABLE_DISK: 
			media = _("Removable Hard Disk"); 
			break;
		case LIBHAL_DRIVE_TYPE_DISK: 
			media = _("Hard Disk"); 
			break;
		case LIBHAL_DRIVE_TYPE_CDROM: 
		{
			switch (libhal_volume_get_disc_type (vol)) {
			default:
			case LIBHAL_VOLUME_DISC_TYPE_CDROM:
				media = _("CD-ROM Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_CDR:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank CD-R Disc");
				else
					media = _("CD-R Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_CDRW:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank CD-RW Disc");
				else
					media = _("CD-RW Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_DVDROM:
				media = _("DVD-ROM Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_DVDRAM:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank DVD-RAM Disc");
				else
					media = _("DVD-RAM Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_DVDR:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank DVD-R Disc");
				else
					media = _("DVD-R Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_DVDRW:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank DVD-RW Disc");
				else
					media = _("DVD-RW Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_DVDPLUSR:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank DVD+R Disc");
				else
					media = _("DVD+R Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_DVDPLUSRW:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank DVD+RW Disc");
				else
					media = _("DVD+RW Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_DVDPLUSR_DL:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank DVD+R Dual Layer Disc");
				else
					media = _("DVD+R Dual Layer Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_BDR:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank BD-R Disc");
				else
					media = _("BD-R Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_BDRE:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank BD-RE Disc");
				else
					media = _("BD-RE Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_HDDVDROM:
				media = _("HD DVD-ROM Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_HDDVDR:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank HD DVD-R Disc");
				else
					media = _("HD DVD-R Disc");
				break;
			case LIBHAL_VOLUME_DISC_TYPE_HDDVDRW:
				if (libhal_volume_disc_is_blank (vol))
					media = _("Blank HD DVD-RW Disc");
				else
					media = _("HD DVD-RW Disc");
				break;
			}
		
			/* Special case for pure audio disc */
			if (libhal_volume_disc_has_audio (vol) && !libhal_volume_disc_has_data (vol)) {
				media = _("Audio Disc");
			}
		}
		break;

		case LIBHAL_DRIVE_TYPE_FLOPPY: 
			media = _("Floppy Disk"); 
			break;
		case LIBHAL_DRIVE_TYPE_TAPE: 
			media = _("Tape"); 
			break;
		case LIBHAL_DRIVE_TYPE_COMPACT_FLASH: 
			media = _("CompactFlash "); 
			break;
		case LIBHAL_DRIVE_TYPE_MEMORY_STICK: 
			media = _("MemoryStick"); 
			break;
		case LIBHAL_DRIVE_TYPE_SMART_MEDIA: 
			media = _("SmartMedia"); 
			break;
		case LIBHAL_DRIVE_TYPE_SD_MMC: 
			media = _("SecureDigital / MultiMediaCard"); 
			break;
		case LIBHAL_DRIVE_TYPE_CAMERA: 
			media = _("Digital Camera"); 
			break;
		case LIBHAL_DRIVE_TYPE_PORTABLE_AUDIO_PLAYER: 
			media = _("Digital Audio Player"); 
			break;
		case LIBHAL_DRIVE_TYPE_ZIP: 
			media = _("Zip"); 
			break;
		case LIBHAL_DRIVE_TYPE_JAZ: 
			media = _("Jaz"); 
			break;
		case LIBHAL_DRIVE_TYPE_FLASHKEY: 
			media = _("Flash Drive"); 
			break;
		default:
			media = _("Unknown Media");
			break;
		}
	}

	mount_point = libhal_volume_get_mount_point (vol);

	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_label")), fslabel);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_uuid")), fsuuid);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_size")), fssize);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_media")), media);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_fs_type")), fstype);
	if (mount_point != NULL) {
		gpointer handle;
		char *mtab_mount_options;
		char *mtab_fstype;
		char *entry_device_file;
		char *entry_mount_options;
		char *entry_fstype;

		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_mount_point")), mount_point);

		mtab_mount_options = NULL;
		mtab_fstype = NULL;

		/* read current mount settings from /etc/mtab or equivalent 
		 * (potentially HAL should know this)
		 */
		if (!mtab_open (&handle)) {
			g_warning ("cannot open mount list");
		}
		while ((mtab_next (handle, &entry_device_file, &entry_mount_options, &entry_fstype))) {
			char *resolved;
			
			resolved = resolve_symlink (entry_device_file);
			g_debug ("/proc/mounts: device %s -> %s \n", entry_device_file, resolved);
			if (strcmp (libhal_volume_get_device_file (vol), resolved) == 0) {
				g_debug ("%s (-> %s) found in mount list. Options '%s', fstype '%s'.\n", 
					 entry_device_file, resolved, entry_mount_options, entry_fstype);
				mtab_mount_options = g_strdup (entry_mount_options);
				mtab_fstype = g_strdup (entry_fstype);
				g_free (resolved);
				break;
			}
			
			g_free (resolved);
		}
		mtab_close (handle);

		if (mtab_mount_options != NULL) {
			int i;

			/* for display, replace comma with space */
			for (i = 0; mtab_mount_options[i] != '\0'; i++) {
				if (mtab_mount_options[i] == ',')
					mtab_mount_options[i] = ' ';
			}

			gtk_label_set_single_line_mode (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_options")), FALSE);
			gtk_label_set_line_wrap (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_options")), TRUE);
			gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_options")), mtab_mount_options);
			
			g_free (mtab_mount_options);
		} else {
			gtk_label_set_markup (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_options")), _("<i>Unknown</i>"));
		}

		if (mtab_fstype != NULL) {
			gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_mounted_fstype")), mtab_fstype);
			g_free (mtab_fstype);
		} else {
			gtk_label_set_markup (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_mounted_fstype")), _("<i>Unknown</i>"));
		}


	} else {
		gtk_label_set_markup (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_mount_point")), _("<i>Not Mounted</i>"));
		gtk_label_set_markup (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_options")), _("<i>Not Mounted</i>"));
		gtk_label_set_markup (GTK_LABEL (glade_xml_get_widget (properties->xml, "volume_mounted_fstype")), _("<i>Not Mounted</i>"));
	}

	/* read settings from gconf */
	properties->keydir = get_keydir (drv, vol);

	populate_ui_from_gconf (properties, 
				"volume_mount_point_entry",
				"volume_mount_options_entry",
				"volume_fstype_entry",
				"volume_expander");

	g_signal_connect (glade_xml_get_widget (properties->xml, "volume_mount_point_entry"), 
                          "changed",
                          G_CALLBACK (mount_point_entry_changed), 
                          properties);
	g_signal_connect (glade_xml_get_widget (properties->xml, "volume_fstype_entry"), 
                          "changed",
                          G_CALLBACK (fstype_entry_changed), 
                          properties);
	g_signal_connect (glade_xml_get_widget (properties->xml, "volume_mount_options_entry"), 
                          "changed",
                          G_CALLBACK (mount_options_entry_changed), 
                          properties);

	g_free (fssize);
	g_free (fstype);
	g_free (fsuuid);
	g_free (fslabel);
	libhal_drive_free (drv);
}



void
gm_properties_view_set_info_drive (GnomeMountPropertiesView *properties, 
				   LibHalDrive              *drv,
				   LibHalContext            *hal_ctx)
{
	GtkWidget *widget;
	char *vendor;
	char *model;
	char *serial;
	char *firmware;
	const char *connection;
	const char *media;
	const char *removable;
	const char *external;
	char buf[128];
	char buf2[128];

	/* Create a new GladeXML object from XML file */
	properties->xml = glade_xml_new (GLADEDIR "/gnome-mount-properties.glade", "gm_page_drive_root", GETTEXT_PACKAGE);
	g_assert (properties->xml != NULL);

	gtk_box_pack_start (GTK_BOX (properties),
			    glade_xml_get_widget (properties->xml, "gm_page_drive_root"),
			    TRUE, TRUE, 0);

	vendor = g_strdup (libhal_drive_get_vendor (drv));
	model = g_strdup (libhal_drive_get_model (drv));
	serial = g_strdup (libhal_drive_get_serial (drv));
	firmware = g_strdup (libhal_drive_get_firmware_version (drv));
	
	switch (libhal_drive_get_bus (drv)) {
	case LIBHAL_DRIVE_BUS_IDE:
		connection = _("ATA");
		break;
	case LIBHAL_DRIVE_BUS_SCSI:
		connection = _("SCSI");
		break;
	case LIBHAL_DRIVE_BUS_USB:
	{
		const char *physdev_udi;
		
		connection = _("USB");
		physdev_udi = libhal_drive_get_physical_device_udi (drv);
		if (physdev_udi != NULL) {
			int version_bcd;
			int speed_bcd;
			char *version;
			char *speed;
			version_bcd = libhal_device_get_property_int (hal_ctx, 
								      physdev_udi,
								      "usb.version_bcd", NULL);
			speed_bcd = libhal_device_get_property_int (hal_ctx, 
								    physdev_udi,
								    "usb.speed_bcd", NULL);
			
			/* TODO: could be a lot smarter about BCD to string conversions */
			switch (version_bcd) {
			case 0x100:
				version = "1.0";
				break;
			case 0x110:
				version = "1.1";
				break;
			case 0x200:
				version = "2.0";
				break;
			default:
				version = NULL;
				break;
			}
			switch (speed_bcd) {
			case 0x150:
				speed = "1.5";
				break;
			case 0x1200:
				speed = "12";
				break;
			case 0x48000:
				speed = "480";
				break;
			default:
				speed = NULL;
				break;
			}
			
			if (version != NULL && speed != NULL) {
				g_snprintf (buf2, sizeof (buf2), "USB %s at %s Mbps", version, speed);
				connection = buf2;
			}
		}
		
	}
	break;
	case LIBHAL_DRIVE_BUS_IEEE1394:
		connection = _("Firewire/IEEE1394");
		break;
	case LIBHAL_DRIVE_BUS_CCW:
		connection = _("CCW");
		break;
	case LIBHAL_DRIVE_BUS_UNKNOWN:
	default:
		connection = _("Unknown Connection");
		break;
	}
	
	switch (libhal_drive_get_type (drv)) {
	case LIBHAL_DRIVE_TYPE_REMOVABLE_DISK: 
		media = _("Removable Hard Disk"); 
		break;
	case LIBHAL_DRIVE_TYPE_DISK: 
		media = _("Hard Disk"); 
		break;
	case LIBHAL_DRIVE_TYPE_CDROM: 
	{
		const char *first;
		const char *second;
		LibHalDriveCdromCaps drive_cdrom_caps;
		
		drive_cdrom_caps = libhal_drive_get_cdrom_caps (drv);
		
		first = _("CD-ROM");
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_CDR)
			first = _("CD-R");
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_CDRW)
			first = _("CD-RW");
		
		second = NULL;
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDROM)
			second = _("DVD-ROM");
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDPLUSR)
			second = _("DVD+R");
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDPLUSRW)
			second = _("DVD+RW");
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDR)
			second = _("DVD-R");
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDRW)
			second = _("DVD-RW");
		if (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDRAM)
			second = _("DVD-RAM");
		if ((drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDR) &&
		    (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDPLUSR))
			second = _("DVD±R");
		if ((drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDRW) &&
		    (drive_cdrom_caps & LIBHAL_DRIVE_CDROM_CAPS_DVDPLUSRW))
			second = _("DVD±RW");
		
		if (second != NULL) {
			g_snprintf (buf, sizeof (buf), _("%s/%s Drive"), first, second);
		} else {
			g_snprintf (buf, sizeof (buf), _("%s Drive"), first);
		}
		media = buf;
	}
	break;
	case LIBHAL_DRIVE_TYPE_FLOPPY: 
		media = _("Floppy Drive"); 
		break;
	case LIBHAL_DRIVE_TYPE_TAPE: 
		media = _("Tape Drive"); 
		break;
	case LIBHAL_DRIVE_TYPE_COMPACT_FLASH: 
		media = _("CompactFlash "); 
		break;
	case LIBHAL_DRIVE_TYPE_MEMORY_STICK: 
		media = _("MemoryStick"); 
		break;
	case LIBHAL_DRIVE_TYPE_SMART_MEDIA: 
		media = _("SmartMedia"); 
		break;
	case LIBHAL_DRIVE_TYPE_SD_MMC: 
		media = _("SecureDigital / MultiMediaCard"); 
		break;
	case LIBHAL_DRIVE_TYPE_CAMERA: 
		media = _("Digital Camera"); 
		break;
	case LIBHAL_DRIVE_TYPE_PORTABLE_AUDIO_PLAYER: 
		media = _("Digital Audio Player"); 
		break;
	case LIBHAL_DRIVE_TYPE_ZIP: 
		media = _("Zip"); 
		break;
	case LIBHAL_DRIVE_TYPE_JAZ: 
		media = _("Jaz"); 
		break;
	case LIBHAL_DRIVE_TYPE_FLASHKEY: 
		media = _("Flash Drive"); 
		break;
	default:
		media = _("Unknown Media");
		break;
	}
	
	if (libhal_drive_uses_removable_media (drv)) {
		if (libhal_drive_requires_eject (drv)) {
			removable = _("Yes (ejectable)");
		} else {
			removable = _("Yes");
		}
	} else {
		removable = _("No");
	}
	
	if (libhal_drive_is_hotpluggable (drv)) {
		external = _("Yes");
	} else {
		external = _("No");
	}
	
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_vendor")), vendor);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_model")), model);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_serial")), serial);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_firmware")), firmware);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_connection")), connection);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_media")), media);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_removable")), removable);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (properties->xml, "drive_external")), external);

	/* read settings from gconf */
	properties->keydir = get_keydir (drv, NULL);

	populate_ui_from_gconf (properties, 
				"drive_mount_point_entry",
				"drive_mount_options_entry",
				"drive_fstype_entry",
				"drive_expander");

	g_signal_connect (glade_xml_get_widget (properties->xml, "drive_mount_point_entry"), 
                          "changed",
                          G_CALLBACK (mount_point_entry_changed), 
                          properties);
	g_signal_connect (glade_xml_get_widget (properties->xml, "drive_fstype_entry"), 
                          "changed",
                          G_CALLBACK (fstype_entry_changed), 
                          properties);
	g_signal_connect (glade_xml_get_widget (properties->xml, "drive_mount_options_entry"), 
                          "changed",
                          G_CALLBACK (mount_options_entry_changed), 
                          properties);
	
	g_free (vendor);
	g_free (model);
	g_free (serial);
	g_free (firmware);
}

static void
gm_properties_view_init (GnomeMountPropertiesView *properties)
{
}

void
gm_properties_view_register_type (GTypeModule *module)
{
	gm_properties_view_get_type ();
}

GtkWidget *
gm_properties_view_new (void)
{
	GnomeMountPropertiesView *properties;

	properties = g_object_new (GM_TYPE_PROPERTIES, NULL);

	return GTK_WIDGET (properties);
}
