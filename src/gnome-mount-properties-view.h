/***************************************************************************
 *
 * gnome-mount-properties-view.h : Nautilus properties page
 *
 * Copyright (C) 2006 David Zeuthen, <david@fubar.dk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 **************************************************************************/

#ifndef __GNOME_MOUNT_PROPERTIES_VIEW_H__
#define __GNOME_MOUNT_PROPERTIES_VIEW_H__

#include <libhal.h>
#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

typedef struct _GnomeMountPropertiesView GnomeMountPropertiesView;
typedef struct _GnomeMountPropertiesViewClass GnomeMountPropertiesViewClass;
typedef struct _GnomeMountPropertiesViewPrivate GnomeMountPropertiesViewPrivate;

#define GM_TYPE_PROPERTIES			(gm_properties_view_get_type())
#define GM_PROPERTIES_VIEW(object)	        (G_TYPE_CHECK_INSTANCE_CAST((object), GM_TYPE_PROPERTIES, GnomeMountPropertiesView))
#define GM_PROPERTIES_VIEW_CLASS(klass)		(G_TYPE_CHACK_CLASS_CAST((klass), GM_TYPE_PROPERTIES, GnomeMountPropertiesViewClass))
#define GM_IS_PROPERTIES_VIEW(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GM_TYPE_PROPERTIES))
#define GM_IS_PROPERTIES_VIEW_CLASS(klass)   	(G_TYPE_CHECK_CLASS_TYPE((klass), GM_TYPE_PROPERTIES))
#define GM_PROPERTIES_VIEW_GET_CLASS(object) 	(G_TYPE_INSTANCE_GET_CLASS((object), GM_TYPE_PROPERTIES, GnomeMountPropertiesViewClass))

GType		gm_properties_view_get_type		(void);
void		gm_properties_view_register_type	(GTypeModule *module);
GtkWidget      *gm_properties_view_new			(void);

void		gm_properties_view_set_info_volume      (GnomeMountPropertiesView *properties, 
							 LibHalVolume             *vol,
							 LibHalContext            *hal_ctx);

void		gm_properties_view_set_info_drive       (GnomeMountPropertiesView *properties, 
							 LibHalDrive              *drv,
							 LibHalContext            *hal_ctx);


G_END_DECLS

#endif /* __GNOME_MOUNT_PROPERTIES_VIEW_H__ */
