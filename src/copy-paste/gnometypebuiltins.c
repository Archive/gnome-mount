#include "gnometypebuiltins.h"
#include "gnome-password-dialog.h"


/* enumerations from "gnome-password-dialog.h" */
static const GEnumValue _gnome_password_dialog_remember_values[] = {
  { GNOME_PASSWORD_DIALOG_REMEMBER_NOTHING, "GNOME_PASSWORD_DIALOG_REMEMBER_NOTHING", "nothing" },
  { GNOME_PASSWORD_DIALOG_REMEMBER_SESSION, "GNOME_PASSWORD_DIALOG_REMEMBER_SESSION", "session" },
  { GNOME_PASSWORD_DIALOG_REMEMBER_FOREVER, "GNOME_PASSWORD_DIALOG_REMEMBER_FOREVER", "forever" },
  { 0, NULL, NULL }
};

GType
gnome_password_dialog_remember_get_type (void)
{
  static GType type = 0;

  if (!type)
    type = g_enum_register_static ("GnomePasswordDialogRemember", _gnome_password_dialog_remember_values);

  return type;
}

