/***************************************************************************
 *
 * gnome-mount-properties.c : Nautilus properties page for gnome-mount
 *
 * Copyright (C) 2006 David Zeuthen, <david@fubar.dk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 **************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <libhal.h>
#include <libhal-storage.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-info-provider.h>
#include <libnautilus-extension/nautilus-property-page-provider.h>

#include "gnome-mount-properties-view.h"

static DBusConnection *dbus_connection;
static LibHalContext *hal_ctx;

static GType gmp_type = 0;
static void property_page_provider_iface_init (NautilusPropertyPageProviderIface *iface);
static GList *gmp_properties_get_pages (NautilusPropertyPageProvider *provider, GList *files);

static void
gnome_mount_properties_plugin_register_type (GTypeModule *module)
{
	static const GTypeInfo info = {
		sizeof (GObjectClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) NULL,
		NULL,
		NULL,
		sizeof (GObject),
		0,
		(GInstanceInitFunc) NULL,
		(const GTypeValueTable *) NULL
	};

	static const GInterfaceInfo property_page_provider_iface_info = {
		(GInterfaceInitFunc) property_page_provider_iface_init,
		NULL,
		NULL
	};

	gmp_type = g_type_module_register_type (module, G_TYPE_OBJECT,
			"GnomeMountPropertiesPlugin",
			&info, 0);

	g_type_module_add_interface (module,
				     gmp_type,
				     NAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER,
				     &property_page_provider_iface_info);
}

static void
property_page_provider_iface_init (NautilusPropertyPageProviderIface *iface)
{
	iface->get_pages = gmp_properties_get_pages;
}


static GList *
gmp_properties_get_pages (NautilusPropertyPageProvider *provider,
			  GList *files)
{
	char *mime;
	GError *error = NULL;
	GList *pages = NULL;
	char *uri = NULL;
	NautilusFileInfo *file;
	GtkWidget *page;
	GtkWidget *label;
	NautilusPropertyPage *property_page;
	char *volume_hal_udi = NULL;
	char *drive_hal_udi = NULL;
	gboolean added_volume;
        GFile *location;

	/* only add properties page if a single file is selected */
	if (files == NULL || files->next != NULL)
		goto out;
	file = files->data;

        /* TODO: There is currently no way to get the GVolume or
         * GDrive for resp. unmounted volumes and drives without media
         * (in computer:///) for a NautilusFileInfo object.
         *
         * http://bugzilla.gnome.org/show_bug.cgi?id=518700
         */

        location = nautilus_file_info_get_location (file);
        drive_hal_udi = NULL; volume_hal_udi = NULL;

        if (nautilus_file_info_get_file_type (file) == G_FILE_TYPE_SHORTCUT ||
            nautilus_file_info_get_file_type (file) == G_FILE_TYPE_MOUNTABLE) {
                char *activation_uri;
                activation_uri = nautilus_file_info_get_activation_uri (file);
                if (activation_uri != NULL) {
                        GFile *activation_file;
                        GVolumeMonitor *volume_monitor;

                        activation_file = g_file_new_for_uri (activation_uri);
                        volume_monitor = g_volume_monitor_get ();
                        if (volume_monitor != NULL) {
                                GList *mounts, *l;
                                mounts = g_volume_monitor_get_mounts (volume_monitor);
                                for (l = mounts; l != NULL; l = l->next) {
                                        GMount *mount = l->data;
                                        GVolume *volume;
                                        GFile *mount_root;

                                        mount_root = g_mount_get_root (mount);
                                        if (g_file_equal (mount_root, activation_file)) {
                                                volume = g_mount_get_volume (mount);
                                                if (volume != NULL) {
                                                        volume_hal_udi = g_volume_get_identifier (volume, 
                                                                                                  G_VOLUME_IDENTIFIER_KIND_HAL_UDI);
                                                        g_object_unref (volume);
                                                }
                                                g_object_unref (mount);
                                        }
                                        g_object_unref (mount_root);
                                }
                                if (mounts != NULL)
                                        g_list_free (mounts);
                        }
                        g_object_unref (volume_monitor);
                        g_object_unref (activation_file);
                        
                }
                g_free (activation_uri);
        }
        g_object_unref (location);

	added_volume = FALSE;
	if (volume_hal_udi != NULL) {
		char *udi;
		LibHalVolume *vol;
		vol = libhal_volume_from_udi (hal_ctx, volume_hal_udi);
		if (vol != NULL) {
                        if (drive_hal_udi == NULL)
                                drive_hal_udi = g_strdup (libhal_volume_get_storage_device_udi (vol));

			page = gm_properties_view_new ();
			gm_properties_view_set_info_volume (GM_PROPERTIES_VIEW (page), vol, hal_ctx);
			label = gtk_label_new (_("Volume"));
			gtk_widget_show (page);
			property_page = nautilus_property_page_new ("gnome-mount-volume-properties", label, page);
			pages = g_list_prepend (pages, property_page);
			libhal_volume_free (vol);
			added_volume = TRUE;
		}
	}

	if (drive_hal_udi != NULL) {
		char *udi;
		const char *udi2;
		LibHalDrive *drv;

		drv = libhal_drive_from_udi (hal_ctx, drive_hal_udi);
		if (drv == NULL && volume_hal_udi != NULL) {
			LibHalVolume *vol;
			vol = libhal_volume_from_udi (hal_ctx, volume_hal_udi);
			if (vol != NULL) {
				udi2 = libhal_volume_get_storage_device_udi (vol);
				drv = libhal_drive_from_udi (hal_ctx, udi2);

				/* oh, so we actually do stem from a volume, it's just not mounted...
				 * show volume dialog anyway then!
				 */
				if (!added_volume) {
					page = gm_properties_view_new ();
					gm_properties_view_set_info_volume (GM_PROPERTIES_VIEW (page), vol, hal_ctx);
					label = gtk_label_new (_("Volume"));
					gtk_widget_show (page);
					property_page = nautilus_property_page_new ("gnome-mount-volume-properties", label, page);
					pages = g_list_prepend (pages, property_page);
				}

				libhal_volume_free (vol);
			}
		}

		if (drv != NULL) {
			page = gm_properties_view_new ();
			gm_properties_view_set_info_drive (GM_PROPERTIES_VIEW (page), drv, hal_ctx);
			label = gtk_label_new (_("Drive"));
			gtk_widget_show (page);
			property_page = nautilus_property_page_new ("gnome-mount-drive-properties", label, page);
			pages = g_list_prepend (pages, property_page);

			libhal_drive_free (drv);
		}
	}

out:

        g_free (drive_hal_udi);
        g_free (volume_hal_udi);

	return pages;
}

/** Internal HAL initialization function
 *
 * @return			The LibHalContext of the HAL connection or
 *				NULL on error.
 */
static LibHalContext *
do_hal_init (void)
{
	LibHalContext *ctx;
	DBusError error;
	char **devices;
	int nr;
	
	ctx = libhal_ctx_new ();
	if (ctx == NULL) {
		g_warning ("Failed to get libhal context");
		goto error;
	}
	
	dbus_error_init (&error);
	dbus_connection = dbus_bus_get (DBUS_BUS_SYSTEM, &error);
	if (dbus_error_is_set (&error)) {
		g_warning ("Cannot connect to system bus: %s : %s", error.name, error.message);
		dbus_error_free (&error);
		goto error;
	}
	
        dbus_connection_setup_with_g_main (dbus_connection, NULL);	
	libhal_ctx_set_dbus_connection (ctx, dbus_connection);
	
	if (!libhal_ctx_init (ctx, &error)) {
		g_warning ("Failed to initialize libhal context: %s : %s", error.name, error.message);
		dbus_error_free (&error);
		goto error;
	}

	return ctx;

error:
	if (ctx != NULL)
		libhal_ctx_free (ctx);
	return NULL;
}

/* Extension module functions.  These functions are defined in 
 * nautilus-extensions-types.h, and must be implemented by all 
 * extensions. */

/* Initialization function.  In addition to any module-specific 
 * initialization, any types implemented by the module should 
 * be registered here. */
void
nautilus_module_initialize (GTypeModule  *module)
{
	//g_print ("Initializing gnome-mount extension\n");

	hal_ctx = do_hal_init ();
	if (hal_ctx == NULL) {
		g_warning ("Could not initialize hal context\n");
		goto error;
	}

	/* set up translation catalog */
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

	gnome_mount_properties_plugin_register_type (module);
	gm_properties_view_register_type (module);

error:
	;
}

/* Perform module-specific shutdown. */
void
nautilus_module_shutdown (void)
{
	//g_print ("Shutting down gnome-mount extension\n");
}

/* List all the extension types.  */
void 
nautilus_module_list_types (const GType **types,
			    int          *num_types)
{
	static GType type_list[1];

	type_list[0] = gmp_type;
	*types = type_list;
	*num_types = G_N_ELEMENTS (type_list);
}
