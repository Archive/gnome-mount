/***************************************************************************
 *
 * gnome-mount.c : GNOME mount, unmount and eject wrappers using HAL
 *
 * Copyright (C) 2006 David Zeuthen, <david@fubar.dk>
 * Copyright 2008 Pierre Ossman
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 **************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>
#include <libhal.h>
#include <libhal-storage.h>

#include <glib/gi18n.h>
#include <gconf/gconf-client.h>
#include <gdk/gdkx.h>
#include "copy-paste/gnome-password-dialog.h"
#include <gnome-keyring.h>
#include <gtk/gtk.h>

#ifdef ENABLE_NOTIFY
#include <libnotify/notify.h>
#endif

#if !defined(NOTIFY_EXPIRES_NEVER)
#define NOTIFY_EXPIRES_NEVER 0
#endif

#if !defined(NOTIFY_EXPIRES_DEFAULT)
#define NOTIFY_EXPIRES_DEFAULT -1
#endif

#if !defined(sun) && !defined(__FreeBSD__)
#include <mntent.h>
#elif defined(__FreeBSD__)
#include <fstab.h>
#include <sys/param.h>
#include <sys/ucred.h>
#include <sys/mount.h>
#elif defined(sun)
#include <sys/mnttab.h>
#endif

static DBusConnection *dbus_connection;
static LibHalContext *hal_ctx;
static GConfClient *gconf_client = NULL;

static char *mount_point_from_command_line = NULL;
static char *mount_options_from_command_line = NULL;
static char *extra_mount_options_from_command_line = NULL;
static char *fstype_from_command_line = NULL;

static int fds[2];
static int rc;
static gboolean opt_noui = FALSE;
static gboolean opt_block = FALSE;
static gboolean opt_nodisplay = FALSE;
static gboolean opt_verbose = FALSE;

#define DBUS_TIMEOUT G_MAXINT

static gboolean
attempt_to_gain_privilege (const char *error_detail)
{
        const char *pk_action;
        const char *pk_result;
        char **tokens;
        gboolean ret;
        DBusGConnection *session_bus;
        DBusGProxy *polkit_gnome_proxy;
        GError *g_error = NULL;
        gboolean gained_privilege;

        ret = FALSE;
        tokens = NULL;
        polkit_gnome_proxy = NULL;

        /* return FALSE if not in GUI mode */
        if (opt_noui)
                goto out;

        tokens = g_strsplit (error_detail, " ", 0);
        if (tokens == NULL)
                goto out;
        if (g_strv_length (tokens) < 2)
                goto out;
        pk_action = tokens[0];
        pk_result = tokens[1];

        g_debug ("pk_action='%s' pk_result='%s'", pk_action, pk_result);

        session_bus = dbus_g_bus_get (DBUS_BUS_SESSION, &g_error);
        if (session_bus == NULL) {
                g_warning ("Caught exception '%s'", g_error->message);
                g_error_free (g_error);
                goto out;
        }
	polkit_gnome_proxy = dbus_g_proxy_new_for_name (session_bus,
                                                        "org.gnome.PolicyKit",           /* bus name */
                                                        "/org/gnome/PolicyKit/Manager",  /* object */
                                                        "org.gnome.PolicyKit.Manager");  /* interface */
        if (polkit_gnome_proxy == NULL) {
                goto out;
        }

        /* now use PolicyKit-gnome to bring up an auth dialog (we
         * don't have any windows so set the XID to "null") 
         */
        if (!dbus_g_proxy_call_with_timeout (polkit_gnome_proxy,
                                             "ShowDialog",
                                             INT_MAX,
                                             &g_error,
                                             /* parameters: */
                                             G_TYPE_STRING, pk_action,      /* action_id */
                                             G_TYPE_UINT, 0,                /* X11 window ID */
                                             G_TYPE_INVALID,
                                             /* return values: */
                                             G_TYPE_BOOLEAN, &gained_privilege,
                                             G_TYPE_INVALID)) {
                g_warning ("Caught exception '%s'", g_error->message);
                g_error_free (g_error);
                goto out;
        }

        ret = gained_privilege;
        g_debug ("gained privilege = %d", gained_privilege);

out:
        if (polkit_gnome_proxy != NULL)
                g_object_unref (polkit_gnome_proxy);
        if (tokens != NULL)
                g_strfreev (tokens);

        return ret;
}

static void 
notify_parent (gboolean success)
{
	static gboolean already_notified = FALSE;

	if (opt_block) {
		rc = success ? 0 : 1;
	} else {
		if (!already_notified) {
			char buf;
			already_notified = TRUE;
			buf = success ? '1' : '0';
			write (fds[1], &buf, 1);
		}
	}
}

static void
show_error_dialog_no_media (const char *udi, LibHalVolume *volume, LibHalDrive *drive)
{
	GtkWidget *w;
	if (!opt_noui) {
		w = gtk_message_dialog_new (NULL, 
					    GTK_DIALOG_MODAL,
					    GTK_MESSAGE_ERROR,
					    GTK_BUTTONS_CLOSE,
					    _("Unable to mount media."));
		/* TODO: use icons, text from gnome-vfs, libhal-storage, e.g. s/drive/floppy drive/ */
		gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (w),
							  _("There is probably no media in the drive."));
		gtk_dialog_run (GTK_DIALOG (w));
		gtk_widget_destroy (w);
	}
}

static void
show_error_dialog_eject (const char *udi, LibHalVolume *volume, LibHalDrive *drive, 
			 const char *error_name, const char *error_detail)
{
	GtkWidget *w;
	if (!opt_noui) {
		const char *volume_name;

		/* We need to handle the following errors 
		 *
		 *   org.freedesktop.Hal.Device.Volume.PermissionDenied
		 *   org.freedesktop.Hal.Device.Volume.UnsupportedEjectOption
		 *   org.freedesktop.Hal.Device.Volume.InvalidEjectOption
		 *   org.freedesktop.Hal.Device.Volume.Busy
		 *   org.freedesktop.Hal.Device.Volume.UnknownFailure
		 *   org.freedesktop.Hal.Device.UnknownError
		 *
		 * in a sane way.
		 */

		w = gtk_message_dialog_new (NULL, 
					    GTK_DIALOG_MODAL,
					    GTK_MESSAGE_ERROR,
					    0,
					    _("Cannot eject volume"));

		if (volume != NULL)
			volume_name = libhal_volume_get_label (volume);
		else
			volume_name = NULL;

		if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.PermissionDenied") == 0) {
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("You are not privileged to eject the volume '%s'.") :
				_("You are not privileged to eject this volume."),
				volume_name);
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.Busy") == 0) {
			/* TODO: figure out exactly which application and find localized name and icon via
			 * desktop files */
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("An application is preventing the volume '%s' from being ejected.") :
				_("An application is preventing the volume from being ejected."),
				volume_name);
			/* TODO: could add 'Lazy Unmount' button */
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.InvalidUnmountOption") == 0 ||
			   strcmp (error_name, "org.freedesktop.Hal.Device.Volume.UnsupportedEjectOption") == 0 ||
			   strcmp (error_name, "org.freedesktop.Hal.Device.Volume.UnknownFailure") == 0 ||
			   strcmp (error_name, "org.freedesktop.Hal.Device.UnknownError") == 0) {
			gtk_message_dialog_format_secondary_markup (
				GTK_MESSAGE_DIALOG (w),
				_("There was an error ejecting the volume or drive."
                                  "\n"
                                  "\n"
                                  "<b>%s:</b> %s"),
                                error_name,
                                error_detail);
		}

		gtk_dialog_add_buttons (GTK_DIALOG (w),
					GTK_STOCK_OK,
					GTK_RESPONSE_NONE,
					NULL);

		gtk_dialog_run (GTK_DIALOG (w));
		gtk_widget_destroy (w);
	}
}

static void
show_error_dialog_unmount (const char *udi, LibHalVolume *volume, LibHalDrive *drive, 
			   const char *error_name, const char *error_detail)
{
	GtkWidget *w;
	gboolean show_details;

	show_details = FALSE;

	if (!opt_noui) {
		const char *volume_name;

		/* We need to handle the following errors 
		 *
		 *   org.freedesktop.Hal.Device.Volume.PermissionDenied
		 *   org.freedesktop.Hal.Device.Volume.UnsupportedUnmountOption
		 *   org.freedesktop.Hal.Device.Volume.InvalidUnmountOption
		 *   org.freedesktop.Hal.Device.Volume.Busy
		 *   org.freedesktop.Hal.Device.Volume.NotMounted
		 *   org.freedesktop.Hal.Device.Volume.UnknownFailure
		 *
		 *
		 * in a sane way.
		 */

		w = gtk_message_dialog_new (NULL, 
					    GTK_DIALOG_MODAL,
					    GTK_MESSAGE_ERROR,
					    0,
					    _("Cannot unmount volume"));

		if (volume != NULL)
			volume_name = libhal_volume_get_label (volume);
		else
			volume_name = NULL;

		if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.PermissionDenied") == 0) {
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("You are not privileged to unmount the volume '%s'.") :
				_("You are not privileged to unmount this volume."),
				volume_name);
			show_details = TRUE;
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.Busy") == 0) {
			/* TODO: figure out exactly which application and find localized name and icon via
			 * desktop files */
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("An application is preventing the volume '%s' from being unmounted.") :
				_("An application is preventing the volume from being unmounted."),
				volume_name);
			/* TODO: could add 'Lazy Unmount' button */
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.NotMounted") == 0 ) {
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("The volume '%s' is not mounted.") :
				_("The volume is not mounted."),
				volume_name);
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.InvalidUnmountOption") == 0 ||
			   strcmp (error_name, "org.freedesktop.Hal.Device.Volume.UnsupportedUnmountOption") == 0 ||
			   strcmp (error_name, "org.freedesktop.Hal.Device.Volume.UnknownFailure") == 0) {
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("Cannot unmount the volume '%s'.") :
				_("Cannot unmount the volume."),
				volume_name);
			show_details = TRUE;
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.NotMountedByHal") == 0) {

			gtk_message_dialog_format_secondary_markup (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ?
				_("The volume '%s' was probably mounted manually on the command line.") :
				_("The volume was probably mounted manually on the command line."),
				volume_name);
			/* show gory details */
			show_details = TRUE;

		} else {
			/* copout fallback; show exception name */

			gtk_message_dialog_format_secondary_markup (
				GTK_MESSAGE_DIALOG (w),
				_("Error <i>%s</i>."),
				error_name);
			/* show gory details */
			show_details = TRUE;
		}

		if (show_details) {
			GtkWidget *expander;
			GtkWidget *elabel;

			expander = gtk_expander_new_with_mnemonic (_("_Details"));
			elabel = gtk_label_new (error_detail);
			gtk_label_set_line_wrap (GTK_LABEL (elabel), TRUE);
			gtk_container_add (GTK_CONTAINER (expander), elabel);
			gtk_container_add (GTK_CONTAINER (GTK_DIALOG(w)->vbox), expander);
			gtk_widget_show (elabel);
			gtk_widget_show (expander);
		}

		gtk_dialog_add_buttons (GTK_DIALOG (w),
					GTK_STOCK_OK,
					GTK_RESPONSE_NONE,
					NULL);

		gtk_dialog_run (GTK_DIALOG (w));
		gtk_widget_destroy (w);
	}
}

/* #define MOUNT_ERROR_DIALOG_RESPONSE_INSTALL_DRIVER 10 see below */

static void
show_error_dialog_mount (LibHalVolume *volume, LibHalDrive *drive,
			 const char *error_name, const char *error_detail, 
			 const char *fstype_requested)
{
	int response;
	const char *volume_name;
	GtkWidget *w;
	gboolean show_details;

	show_details = FALSE;

	/* only some errors want to show the expander with all the gory details */

	if (!opt_noui) {
		/* We need to handle the following errors 
		 *
		 *   org.freedesktop.Hal.Device.Volume.PermissionDenied
		 *   org.freedesktop.Hal.Device.Volume.InvalidMountOption
		 *   org.freedesktop.Hal.Device.Volume.FailedToCreateMountpoint
		 *   org.freedesktop.Hal.Device.Volume.UnknownFilesystemType
		 *   org.freedesktop.Hal.Device.Volume.UnknownFailure
		 *   org.freedesktop.Hal.Device.Volume.AlreadyMounted
		 *
		 * in a sane way (TODO: review hal exceptions, and handle the ones we don't handle here!).
		 *
		 */

		w = gtk_message_dialog_new (NULL, 
					    GTK_DIALOG_MODAL,
					    GTK_MESSAGE_ERROR,
					    0,
					    _("Cannot mount volume."));

		if (volume != NULL)
			volume_name = libhal_volume_get_label (volume);
		else
			volume_name = NULL;

		if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.PermissionDenied") == 0) {
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("You are not privileged to mount the volume '%s'.") :
				_("You are not privileged to mount this volume."),
				volume_name);
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.InvalidMountOption") == 0) {
			/* TODO: slim down mount options to what is allowed, cf. volume.mount.valid_options */
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("Invalid mount option when attempting to mount the volume '%s'.") :
				_("Invalid mount option when attempting to mount the volume."),
				volume_name);
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.UnknownFilesystemType") == 0) {
			gtk_message_dialog_format_secondary_markup (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("The volume '%s' uses the <i>%s</i> file system which is not supported by your system.") :
				_("The volume uses the <i>%s</i> file system which is not supported by your system."),
				volume_name != NULL ? volume_name : (fstype_requested != NULL ? fstype_requested : ""),
				volume_name != NULL ? (fstype_requested != NULL ? fstype_requested : "") : "");

			/* some day.. :-)
			gtk_dialog_add_buttons (GTK_DIALOG (w),
						_("Install Driver..."),
						MOUNT_ERROR_DIALOG_RESPONSE_INSTALL_DRIVER,
						NULL);
			*/
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.FailedToCreateMountPoint") == 0 ||
			   strcmp (error_name, "org.freedesktop.Hal.Device.Volume.UnknownFailure") == 0) {

			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("Unable to mount the volume '%s'.") :
				_("Unable to mount the volume."),
				volume_name);
			/* show gory details */
			show_details = TRUE;
			
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.Volume.AlreadyMounted") == 0) {

			/* don't show any dialog for this */
			gtk_widget_destroy (w);
			w = NULL;

#if 0
			gtk_message_dialog_format_secondary_text (
				GTK_MESSAGE_DIALOG (w),
				volume_name != NULL ? 
				_("Volume '%s' is already mounted.") :
				_("Volume is already mounted."),
				volume_name);
			/* show gory details */
			show_details = TRUE;
#endif
		} else if (strcmp (error_name, "org.freedesktop.Hal.Device.InterfaceLocked") == 0) {
			/* this happens when someone (such as gparted or anaconda) is locking
			 *
			 * Don't show any dialog for this; because
			 * it's probably locked due to partitioning
			 * and the volume contains signatures are not
			 * yet cleaned out...
			 */
			gtk_widget_destroy (w);
			w = NULL;
			 
		} else if (strcmp (error_name, "org.freedesktop.DBus.Error.UnknownMethod") == 0) {
			/* happens when HAL don't support the Mount() method - which we only 
			 * do for volumes with mountable file systems
			 *
			 * Don't show any dialog for this.
			 */
			gtk_widget_destroy (w);
			w = NULL;
			 
		} else {

			/* copout fallback; show exception name */

			gtk_message_dialog_format_secondary_markup (
				GTK_MESSAGE_DIALOG (w),
				_("Error <i>%s</i>."),
				error_name);
			/* show gory details */
			show_details = TRUE;
			
		}

		if (w != NULL) {
			if (show_details) {
				GtkWidget *expander;
				GtkWidget *elabel;
				
				expander = gtk_expander_new_with_mnemonic (_("_Details"));
				elabel = gtk_label_new (error_detail);
				gtk_label_set_line_wrap (GTK_LABEL (elabel), TRUE);
				gtk_container_add (GTK_CONTAINER (expander), elabel);
				gtk_container_add (GTK_CONTAINER (GTK_DIALOG(w)->vbox), expander);
				gtk_widget_show (elabel);
				gtk_widget_show (expander);
			}
			
			gtk_dialog_add_buttons (GTK_DIALOG (w),
						GTK_STOCK_OK,
						GTK_RESPONSE_NONE,
						NULL);
			
			response = gtk_dialog_run (GTK_DIALOG (w));
			gtk_widget_destroy (w);
			
			switch (response) {
				/* case MOUNT_ERROR_DIALOG_RESPONSE_INSTALL_DRIVER:
				   g_message ("install driver!");
				   break; */
			default:
				break;
			}
		}

	}
}


/** Integrate a dbus mainloop. 
 *
 *  @param  ctx                 LibHal context
 *  @param  error     		pointer to a D-BUS error object
 *
 *  @return 			TRUE if we connected to the bus
 */
static dbus_bool_t 
hal_mainloop_integration (LibHalContext *ctx, DBusError *error)
{
	return FALSE;
}

/** Internal HAL initialization function
 *
 * @return			The LibHalContext of the HAL connection or
 *				NULL on error.
 */
static LibHalContext *
do_hal_init (void)
{
	LibHalContext *ctx;
	DBusError error;
	char **devices;
	int nr;
	
	ctx = libhal_ctx_new ();
	if (ctx == NULL) {
		g_warning ("Failed to get libhal context");
		goto error;
	}
	
	dbus_error_init (&error);
	dbus_connection = dbus_bus_get (DBUS_BUS_SYSTEM, &error);
	if (dbus_error_is_set (&error)) {
		g_warning ("Cannot connect to system bus: %s : %s", error.name, error.message);
		dbus_error_free (&error);
		goto error;
	}
	
        dbus_connection_setup_with_g_main (dbus_connection, NULL);	
	libhal_ctx_set_dbus_connection (ctx, dbus_connection);
	
	if (!libhal_ctx_init (ctx, &error)) {
		g_warning ("Failed to initialize libhal context: %s : %s", error.name, error.message);
		dbus_error_free (&error);
		goto error;
	}

	return ctx;

error:
	if (ctx != NULL)
		libhal_ctx_free (ctx);
	return NULL;
}

static LibHalVolume *
volume_findby (LibHalContext *hal_ctx, const char *property, const char *value)
{
	int i;
	char **hal_udis;
	int num_hal_udis;
	LibHalVolume *result = NULL;
	char *found_udi = NULL;
	DBusError error;

	dbus_error_init (&error);
	if ((hal_udis = libhal_manager_find_device_string_match (hal_ctx, property, 
								 value, &num_hal_udis, &error)) == NULL)
		goto out;

	for (i = 0; i < num_hal_udis; i++) {
		char *udi;
		udi = hal_udis[i];
		if (libhal_device_query_capability (hal_ctx, udi, "volume", &error)) {
			found_udi = strdup (udi);
			break;
		}
	}

	libhal_free_string_array (hal_udis);

	if (found_udi != NULL)
		result = libhal_volume_from_udi (hal_ctx, found_udi);

	free (found_udi);
out:
	return result;
}

static LibHalVolume *
volume_from_nickname (LibHalContext *hal_ctx, const char *name)
{
	int name_len;
	char *path;
	LibHalVolume *result = NULL;

	if (name[0] == '/') {
		result = volume_findby (hal_ctx, "volume.mount_point", name);
	}

	name_len = strlen (name);
	if ((result == NULL) && ((path = calloc (1, sizeof ("/media/") + name_len + 1)) != NULL)) {
		strcat(path, "/media/");
		strcat(path, name);
		result = volume_findby (hal_ctx, "volume.mount_point", path);
		free(path);
	}

	if (result == NULL) {
		result = volume_findby (hal_ctx, "volume.label", name);
	}

	return (result);
}

static const char *
get_dev_file (LibHalVolume *volume, LibHalDrive *drive)
{
	if (volume != NULL) {
		return libhal_volume_get_device_file (volume);
	} else if (drive != NULL) {
		return libhal_drive_get_device_file (drive);
	} else
		return NULL;
}

static char *
get_mntent_mount_point(const char *device_file)
{
	char *mount_point;
#if! defined(sun) && !defined(__FreeBSD__)
	FILE *f;
	struct mntent mnt;
	struct mntent *mnte;
	char buf[512];
#elif defined(__FreeBSD__)
	struct statfs *mounts;
	int n_mounts;
	int i;
#elif defined(sun)
	FILE *f;
	struct mnttab mnt;
	struct mnttab mpref = { NULL, NULL, NULL, NULL, NULL };
#endif

	mount_point = NULL;

#if !defined(sun) && !defined(__FreeBSD__)
	if ((f = setmntent ("/proc/mounts", "r")) != NULL) {
		
		while ((mnte = getmntent_r (f, &mnt, buf, sizeof(buf))) != NULL) {
			if (strcmp (mnt.mnt_fsname, device_file) == 0) {
				if (mnt.mnt_dir != NULL) {
					mount_point = g_strdup (mnt.mnt_dir);
				}
				break;
			}
		}
		endmntent (f);
	}

#elif defined(sun) /* sun */

	if ((f = fopen(MNTTAB, "r")) != NULL) {
		mpref.mnt_special = (char *)device_file;
		if (getmntany(f, &mnt, &mpref) == 0) {
			mount_point = g_strdup (mnt.mnt_mountp);
		}
		fclose(f);
	}
#elif defined(__FreeBSD__)
	n_mounts = getmntinfo(&mounts, MNT_NOWAIT);
	for (i = 0; i < n_mounts; i++) {
		if (!strcmp(mounts[i].f_mntfromname, device_file))
			mount_point = g_strdup (mounts[i].f_mntonname);
	}
#endif /* sun && __FreeBSD__ */

out:
	return (mount_point);
}

static gboolean
volume_mount_with_options (const char *udi, LibHalVolume *volume, LibHalDrive *drive,
			   const char *mount_point, const char *fstype, GPtrArray *options)
{
	DBusMessage *reply = NULL;
	DBusMessage *dmesg = NULL;
	gboolean ret = FALSE;
	DBusError error;
	unsigned int i;
	const char *device_file;
	char *mounted_at;
	
	if (mount_point == NULL)
		mount_point = "";

	if (fstype == NULL)
		fstype = "";
	
	g_debug ("Mounting %s with mount_point='%s', fstype='%s', num_options=%d", 
		   udi,
		   mount_point, 
		   fstype,
		   options->len);
	for (i = 0; i < options->len; i++)
		g_debug ("  option='%s'", (char *) options->pdata[i]);
	
	if (!(dmesg = dbus_message_new_method_call ("org.freedesktop.Hal", udi,
						    "org.freedesktop.Hal.Device.Volume",
						    "Mount"))) {
		g_warning ("Could not create dbus message for %s", udi);
		return FALSE;
	}
	
	if (!dbus_message_append_args (dmesg, DBUS_TYPE_STRING, &mount_point, DBUS_TYPE_STRING, &fstype,
				       DBUS_TYPE_ARRAY, DBUS_TYPE_STRING, &options->pdata, options->len,
				       DBUS_TYPE_INVALID)) {
		g_warning ("Could not append args to dbus message for %s", udi);
		dbus_message_unref (dmesg);
		return FALSE;
	}

	dbus_error_init (&error);
	if (!(reply = dbus_connection_send_with_reply_and_block (dbus_connection, dmesg, DBUS_TIMEOUT, &error)) ||
	    dbus_error_is_set (&error)) {
                if (opt_verbose)
                        g_message ("Mount failed for %s\n%s : %s\n", udi, error.name, error.message);

		if (strcmp (error.name, "org.freedesktop.Hal.Device.Volume.InvalidMountpoint") == 0 &&
		    mount_point != NULL) {

			dbus_message_unref (dmesg);
			if (reply != NULL)
				dbus_message_unref (reply);

                        if (opt_verbose)
                                g_message ("Given mount point name '%s' was invalid, trying without this...", mount_point);

			/* possibly illegal mount point name; try without that mount point name... */
			ret = volume_mount_with_options (udi, volume, drive, NULL, fstype, options);
			if (ret) {
				/* TODO: log to syslog that given mount_point was illegal */
			}

			dbus_error_free (&error);

			return ret;
		} else if (strcmp (error.name, "org.freedesktop.Hal.Device.Volume.MountPointNotAvailable") == 0 &&
			   mount_point != NULL) {
			char *new_mount_point;

			dbus_message_unref (dmesg);
			if (reply != NULL)
				dbus_message_unref (reply);

			new_mount_point = g_strdup_printf ("%s_", mount_point);

                        if (opt_verbose)
                                g_message ("Given mount point name '%s' is unavailable, trying with '%s'...", 
                                           mount_point, new_mount_point);

			/* possibly illegal mount point name; try without that mount point name... */
			ret = volume_mount_with_options (udi, volume, drive, new_mount_point, fstype, options);
			if (ret) {
				/* TODO: log to syslog that given mount_point was illegal */
			}

			g_free (new_mount_point);

			dbus_error_free (&error);

			return ret;
		} else if (strcmp (error.name, "org.freedesktop.Hal.Device.PermissionDeniedByPolicy") == 0) {
                        /* attempt to gain privilege */
                        if (attempt_to_gain_privilege (error.message)) {
                                ret = volume_mount_with_options (udi, volume, drive, NULL, fstype, options);
                        }
                        dbus_error_free (&error);
                        return ret;
                }

		notify_parent (FALSE);

		show_error_dialog_mount (volume, drive, error.name, error.message, 
					 (fstype != NULL && strlen (fstype) > 0) ? 
					 fstype : (volume != NULL ? libhal_volume_get_fstype (volume) : NULL));

		dbus_error_free (&error);
		goto out;
	}

	if ((device_file = get_dev_file (volume, drive)) != NULL) {
		if ((mounted_at = get_mntent_mount_point(device_file)) != NULL) {
                        if (opt_verbose)
                                g_print (_("Mounted %s at \"%s\"\n"), device_file, mounted_at);
			g_free (mounted_at);
		}
	}

	ret = TRUE;

out:
	if (dmesg != NULL)
		dbus_message_unref (dmesg);
	if (reply != NULL)
		dbus_message_unref (reply);
	
	return ret;
}


static gboolean
fstab_open (gpointer *handle)
{
#ifdef __FreeBSD__
	return setfsent () == 1;
#else
	*handle = fopen ("/etc/fstab", "r");
	return *handle != NULL;
#endif
}

static char *
fstab_next (gpointer handle, char **mount_point)
{
#ifdef __FreeBSD__
	struct fstab *fstab;

	fstab = getfsent ();

	/* TODO: fill out mount_point */
	if (mount_point != NULL && fstab != NULL) {
		*mount_point = fstab->fs_file;
	}

	return fstab ? fstab->fs_spec : NULL;
#else
	struct mntent *mnt;

	mnt = getmntent (handle);

	if (mount_point != NULL && mnt != NULL) {
		*mount_point = mnt->mnt_dir;
	}

	return mnt ? mnt->mnt_fsname : NULL;
#endif
}


static void
fstab_close (gpointer handle)
{
#ifdef __FreeBSD__
	endfsent ();
#else
	fclose (handle);
#endif
}


/* borrowed from gtk/gtkfilesystemunix.c in GTK+ on 02/23/2006 */
static void
canonicalize_filename (gchar *filename)
{
	gchar *p, *q;
	gboolean last_was_slash = FALSE;
	
	p = filename;
	q = filename;
	
	while (*p)
	{
		if (*p == G_DIR_SEPARATOR)
		{
			if (!last_was_slash)
				*q++ = G_DIR_SEPARATOR;
			
			last_was_slash = TRUE;
		}
		else
		{
			if (last_was_slash && *p == '.')
			{
				if (*(p + 1) == G_DIR_SEPARATOR ||
				    *(p + 1) == '\0')
				{
					if (*(p + 1) == '\0')
						break;
					
					p += 1;
				}
				else if (*(p + 1) == '.' &&
					 (*(p + 2) == G_DIR_SEPARATOR ||
					  *(p + 2) == '\0'))
				{
					if (q > filename + 1)
					{
						q--;
						while (q > filename + 1 &&
						       *(q - 1) != G_DIR_SEPARATOR)
							q--;
					}
					
					if (*(p + 2) == '\0')
						break;
					
					p += 2;
				}
				else
				{
					*q++ = *p;
					last_was_slash = FALSE;
				}
			}
			else
			{
				*q++ = *p;
				last_was_slash = FALSE;
			}
		}
		
		p++;
	}
	
	if (q > filename + 1 && *(q - 1) == G_DIR_SEPARATOR)
		q--;
	
	*q = '\0';
}

static char *
resolve_symlink (const char *file)
{
	GError *error;
	char *dir;
	char *link;
	char *f;
	char *f1;

	f = g_strdup (file);

	while (g_file_test (f, G_FILE_TEST_IS_SYMLINK)) {
		link = g_file_read_link (f, &error);
		if (link == NULL) {
			g_warning ("Cannot resolve symlink %s: %s", f, error->message);
			g_error_free (error);
			g_free (f);
			f = NULL;
			goto out;
		}
		
		dir = g_path_get_dirname (f);
		f1 = g_strdup_printf ("%s/%s", dir, link);
		g_free (dir);
		g_free (link);
		g_free (f);
		f = f1;
	}

out:
	if (f != NULL)
		canonicalize_filename (f);
	return f;
}

static gboolean
is_in_fstab (const char *device_file, const char *label, const char *uuid, char **mount_point)
{
	gboolean ret;
	gpointer handle;
	char *entry;
	char *_mount_point;

	ret = FALSE;

	/* check if /etc/fstab mentions this device... (with symlinks etc) */
	if (!fstab_open (&handle)) {
		handle = NULL;
		goto out;
	}

	while ((entry = fstab_next (handle, &_mount_point)) != NULL) {
		char *resolved;

		if (label != NULL && g_str_has_prefix (entry, "LABEL=")) {
			if (strcmp (entry + 6, label) == 0) {
				gboolean skip_fstab_entry;

				skip_fstab_entry = FALSE;

				/* OK, so what's if someone attaches an external disk with the label '/' and 
				 * /etc/fstab has
				 *
				 *    LABEL=/    /    ext3    defaults    1 1
				 *
				 * in /etc/fstab as most Red Hat systems do? Bugger, this is a very common use
				 * case; suppose that you take the disk from your Fedora server and attaches it
				 * to your laptop. Bingo, you now have two disks with the label '/'. One must
				 * seriously wonder if using things like LABEL=/ for / is a good idea; just
				 * what happens if you boot in this configuration? (answer: the initrd gets
				 * it wrong most of the time.. sigh)
				 *
				 * To work around this, check if the listed entry in /etc/fstab is already mounted,
				 * if it is, then check if it's the same device_file as the given one...
				 */

				/* see if a volume is mounted at this mount point  */
				if (_mount_point != NULL) {
					LibHalVolume *mounted_vol;

					mounted_vol = volume_findby (hal_ctx, "volume.mount_point", _mount_point);
					if (mounted_vol != NULL) {
						const char *mounted_vol_device_file;

						mounted_vol_device_file = libhal_volume_get_device_file (mounted_vol);
						/* no need to resolve symlinks, hal uses the canonical device file */
						g_debug ("device_file = '%s'", device_file);
						g_debug ("mounted_vol_device_file = '%s'", mounted_vol_device_file);
						if (mounted_vol_device_file != NULL &&
						    strcmp (mounted_vol_device_file, device_file) !=0) {

							g_debug ("Wanting to mount %s that has label %s, but /etc/fstab says LABEL=%s is to be mounted at mount point '%s'. However %s (that also has label %s), is already mounted at said mount point. So, skipping said /etc/fstab entry.\n", 
								   device_file, label, label, _mount_point, mounted_vol_device_file, _mount_point);
							skip_fstab_entry = TRUE;
						}
						libhal_volume_free (mounted_vol);
					}

				}

				if (!skip_fstab_entry) {
					ret = TRUE;
					if (mount_point != NULL)
						*mount_point = g_strdup (_mount_point);
					goto out;
				}
			}
		} 

		if (uuid != NULL && g_str_has_prefix (entry, "UUID=")) {
			if (strcmp (entry + 5, uuid) == 0) {
				ret = TRUE;
				if (mount_point != NULL)
					*mount_point = g_strdup (_mount_point);
				goto out;
			}
		} 

		resolved = resolve_symlink (entry);
		if (strcmp (device_file, resolved) == 0) {
			ret = TRUE;
			g_free (resolved);
			if (mount_point != NULL)
				*mount_point = g_strdup (_mount_point);
			goto out;
		}

		g_free (resolved);
	}

out:
	if (handle != NULL)
		fstab_close (handle);

	return ret;
}

#ifdef __FreeBSD__
#define MOUNT		"/sbin/mount"
#define UMOUNT		"/sbin/umount"
#else
#define MOUNT		"/bin/mount"
#define UMOUNT		"/bin/umount"
#endif

static gboolean
volume_mount (const char *udi, LibHalVolume *volume, LibHalDrive *drive)
{
	char **strlist;
	char uidbuf[64];
	char *mount_point;
	char *fstype_override;
	GPtrArray *mount_options;
	char *key;
	gboolean ret;
	const char *fstype;
	const char *device_file;
	const char *label;
	const char *uuid;

	ret = FALSE;

	g_debug ("Mounting %s", udi);

	/* check if it's in /etc/fstab */
	label = NULL;
	uuid = NULL;
	device_file = NULL;
	if (volume != NULL) {
		label = libhal_volume_get_label (volume);
		uuid = libhal_volume_get_uuid (volume);
		device_file = libhal_volume_get_device_file (volume);
	} else if (drive != NULL) {
		device_file = libhal_drive_get_device_file (drive);
	}
	if (device_file != NULL) {
		char *mount_point_fstab = NULL;

		if (is_in_fstab (device_file, label, uuid, &mount_point_fstab)) {
			GError *err = NULL;
			char *sout = NULL;
			char *serr = NULL;
			int exit_status;
			char *args[3] = {MOUNT, NULL, NULL};
			char **envp = {NULL};

			/* don't mount if device is locked
                         */
                        if (libhal_device_is_locked_by_others (hal_ctx,
                                                               udi,
                                                               "org.freedesktop.Hal.Device.Storage",
                                                               NULL)) {
                                if (opt_verbose)
                                        g_message (_("Device %s is locked, aborting"),
                                                   udi);
                                goto out;
                        }

                        if (opt_verbose)
                                g_print (_("Device %s is in /etc/fstab with mount point \"%s\"\n"), 
                                         device_file, mount_point_fstab);
			args[1] = mount_point_fstab;
			if (!g_spawn_sync ("/",
					   args,
					   envp,
					   0,
					   NULL,
					   NULL,
					   &sout,
					   &serr,
					   &exit_status,
					   &err)) {
				g_warning ("Cannot execute %s\n", MOUNT);
				g_free (mount_point_fstab);
				goto out;
			}

			if (exit_status != 0) {
				char errstr[] = "mount: unknown filesystem type";

                                if (opt_verbose)
                                        g_message ("%s said error %d, stdout='%s', stderr='%s'\n", 
                                                   MOUNT, exit_status, sout, serr);

				notify_parent (FALSE);

				if (strstr (serr, "unknown filesystem type") != NULL) {
					show_error_dialog_mount (volume, drive, "org.freedesktop.Hal.Device.Volume.UnknownFilesystemType", serr, NULL);
				} else if (strstr (serr, "already mounted") != NULL) {
					show_error_dialog_mount (volume, drive, "org.freedesktop.Hal.Device.Volume.AlreadyMounted", serr, NULL);
				} else if (strstr (serr, "only root") != NULL) {
					show_error_dialog_mount (volume, drive, "org.freedesktop.Hal.Device.Volume.PermissionDenied", serr, NULL);
				} else if (strstr (serr, "bad option") != NULL) {
					show_error_dialog_mount (volume, drive, "org.freedesktop.Hal.Device.Volume.InvalidMountOption", serr, NULL);
				} else {
					show_error_dialog_mount (volume, drive, "org.freedesktop.Hal.Device.Volume.UnknownFailure", serr, NULL);
				}

				g_free (mount_point_fstab);
				goto out;

			}

                        if (opt_verbose)
                                g_print (_("Mounted %s at \"%s\" (using /etc/fstab)\n"), device_file, mount_point_fstab);

			g_free (mount_point_fstab);
			ret = TRUE;
			goto out;

		}
		g_free (mount_point_fstab);
	}



	if (volume != NULL) {
		fstype = libhal_volume_get_fstype (volume);
	} else {
		fstype = NULL;
	}
	
	mount_options = g_ptr_array_new ();
	mount_point = NULL;
	fstype_override = NULL;
			
	/* read per-drive settings from gconf and adjust settings */
	const char *drive_udi;
	drive_udi = NULL;
	if (volume != NULL) {
		drive_udi = libhal_volume_get_storage_device_udi (volume);
	} else if (drive != NULL) {
		drive_udi = libhal_drive_get_udi (drive);
	}
	if (drive_udi != NULL) {
		int n;
		GSList *options_list;
		GSList *l;
		GSList *next;
		char *udi2;
		char *mount_point_drv;
		char *fstype_override_drv;

		udi2 = g_strdup (drive_udi);
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}

		key = g_strdup_printf ("/system/storage/drives/%s/mount_point", udi2);
		mount_point_drv = gconf_client_get_string (gconf_client, key, NULL);
		if (mount_point_drv != NULL) {
			g_debug ("read mount point '%s' from gconf string key '%s'", mount_point_drv, key);
			g_free (mount_point);
			mount_point = mount_point_drv;
			
		}
		g_free (key);

		key = g_strdup_printf ("/system/storage/drives/%s/mount_options", udi2);
		if ((options_list = gconf_client_get_list (gconf_client, key, GCONF_VALUE_STRING, NULL)) != NULL) {

			g_ptr_array_free (mount_options, TRUE);
			mount_options = g_ptr_array_new ();

			for (l = options_list; l != NULL; l = next) {
				char *option;

				next = l->next;
				option = l->data;

				g_debug ("read option '%s' from gconf string key '%s'", option, key);
				g_ptr_array_add (mount_options, option);

				g_slist_free_1 (l);
			}
		}
		g_free (key);

		key = g_strdup_printf ("/system/storage/drives/%s/fstype_override", udi2);
		fstype_override_drv = gconf_client_get_string (gconf_client, key, NULL);
		if (fstype_override_drv != NULL) {
			g_debug ("read fstype_override '%s' from gconf string key '%s'", fstype_override_drv, key);
			g_free (fstype_override);
			fstype_override = fstype_override_drv;
			
		}
		g_free (key);

		g_free (udi2);
	}

	/* read legacy settings from HAL properties (GNOME bug #345546) 
	 * 
	 * We *could* also read volume.policy.mount_option.* properties but...
	 */
	if (volume != NULL) {
		char *mount_point_from_hal;
		const char *drive_udi;

		drive_udi = libhal_volume_get_storage_device_udi (volume);
		if (drive_udi != NULL) {
			mount_point_from_hal = libhal_device_get_property_string (hal_ctx, 
										  drive_udi,
										  "storage.policy.desired_mount_point",
										  NULL);
			if (mount_point_from_hal != NULL) {
				g_free (mount_point);
				mount_point = g_strdup (mount_point_from_hal);
				libhal_free_string (mount_point_from_hal);
				g_debug ("read mount point '%s' from legacy hal property 'storage.policy.desired_mount_point'", 
					 mount_point);
			}
		}

		mount_point_from_hal = libhal_device_get_property_string (hal_ctx, 
									  libhal_volume_get_udi (volume),
									  "volume.policy.desired_mount_point",
									  NULL);
		if (mount_point_from_hal != NULL) {
			g_free (mount_point);
			mount_point = g_strdup (mount_point_from_hal);
			libhal_free_string (mount_point_from_hal);
			g_debug ("read mount point '%s' from legacy hal property 'volume.policy.desired_mount_point'", 
				 mount_point);
		}
	}

	/* read per-volume settings from gconf and adjust if found */
	if (volume != NULL && gconf_client != NULL) {
		int n;
		GSList *options_list;
		GSList *l;
		GSList *next;
		char *udi2;
		char *mount_point_vol;
		char *fstype_override_vol;

		udi2 = g_strdup (libhal_volume_get_udi (volume));
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}

		key = g_strdup_printf ("/system/storage/volumes/%s/mount_point", udi2);
		mount_point_vol = gconf_client_get_string (gconf_client, key, NULL);
		if (mount_point_vol != NULL) {
			g_debug ("read mount point '%s' from gconf string key '%s'", mount_point_vol, key);
			g_free (mount_point);
			mount_point = mount_point_vol;
			
		}
		g_free (key);

		key = g_strdup_printf ("/system/storage/volumes/%s/mount_options", udi2);
		if ((options_list = gconf_client_get_list (gconf_client, key, GCONF_VALUE_STRING, NULL)) != NULL) {

			g_ptr_array_free (mount_options, TRUE);
			mount_options = g_ptr_array_new ();

			for (l = options_list; l != NULL; l = next) {
				char *option;

				next = l->next;
				option = l->data;

				g_debug ("read option '%s' from gconf string key '%s'", option, key);
				g_ptr_array_add (mount_options, option);

				g_slist_free_1 (l);
			}
		}
		g_free (key);

		key = g_strdup_printf ("/system/storage/volumes/%s/fstype_override", udi2);
		fstype_override_vol = gconf_client_get_string (gconf_client, key, NULL);
		if (fstype_override_vol != NULL) {
			g_debug ("read fstype_override '%s' from gconf string key '%s'", fstype_override_vol, key);
			g_free (fstype_override);
			fstype_override = fstype_override_vol;
			
		}
		g_free (key);

		g_free (udi2);
	}


	/* override mount_point from commandline, if given */
	if (mount_point_from_command_line != NULL) {
		g_free (mount_point);
		mount_point = g_strdup (mount_point_from_command_line);
	}

	/* override mount_options from commandline, if given */
	if (mount_options_from_command_line != NULL) {
		int i;
		char **opt_from_cmd_line;

		g_ptr_array_free (mount_options, TRUE);
		mount_options = g_ptr_array_new ();

		opt_from_cmd_line = g_strsplit (mount_options_from_command_line, ",", 0);
		for (i = 0; opt_from_cmd_line[i] != NULL; i++) {
			g_ptr_array_add (mount_options, g_strdup (opt_from_cmd_line[i]));
		}
		g_strfreev (opt_from_cmd_line);
	}

	/* add extra_mount_options from commandline, if given */
	if (extra_mount_options_from_command_line != NULL) {
		int i;
		char **extra_opt_from_cmd_line;

		extra_opt_from_cmd_line = g_strsplit (extra_mount_options_from_command_line, ",", 0);
		for (i = 0; extra_opt_from_cmd_line[i] != NULL; i++) {
			g_ptr_array_add (mount_options, g_strdup (extra_opt_from_cmd_line[i]));
		}
		g_strfreev (extra_opt_from_cmd_line);
	}

	/* override fstype from commandline, if given */
	if (fstype_from_command_line != NULL) {
		g_free (fstype_override);
		fstype_override = g_strdup (fstype_from_command_line);
	}

	/* let a file system specify the override unless set already; e.g. for ntfs we want to override to ntfs-3g */
	if (fstype_override == NULL) {
		key = g_strdup_printf ("/system/storage/default_options/%s/fstype_override", fstype);
		fstype_override = gconf_client_get_string (gconf_client, key, NULL);
		g_free (key);
		if (fstype_override != NULL && strlen (fstype_override) == 0) {
			g_free (fstype_override);
			fstype_override = NULL;
		}
	}

	if (volume == NULL && (mount_options->len == 0)) {
		/* volume from a non-pollable drive, just set uid.. */
		
#ifndef __FreeBSD__
		snprintf (uidbuf, sizeof (uidbuf) - 1, "uid=%u", getuid ());
#else
		snprintf (uidbuf, sizeof (uidbuf) - 1, "-u=%u", getuid ());
#endif
		g_ptr_array_add (mount_options, uidbuf);
		
	} else if (((fstype_override != NULL) || (fstype != NULL)) && (mount_options->len == 0)) {
		GSList *options_list;
		GSList *l;
		GSList *next;
		const char *fstype_to_use;

		if (fstype_override != NULL)
			fstype_to_use = fstype_override;
		else
			fstype_to_use = fstype;

		/* read default mount options per file system (unless options are already retrieved from 
		 * somewhere else) */
		key = g_strdup_printf ("/system/storage/default_options/%s/mount_options", fstype_to_use);
		if ((options_list = gconf_client_get_list (gconf_client, key, GCONF_VALUE_STRING, NULL)) != NULL) {
			for (l = options_list; l != NULL; l = next) {
				char *option;

				next = l->next;
				option = l->data;

				g_debug ("read default option '%s' from gconf strlist key %s", option, key);

				/* special workaround to replace "uid=" with "uid=<actual uid of caller>" */
#ifndef __FreeBSD__
				if (strcmp (option, "uid=") == 0) {
					g_free (option);
					option = g_strdup_printf ("uid=%u", getuid ());
				}
#else
				if (strcmp (option, "-u=") == 0) {
					g_free (option);
					option = g_strdup_printf ("-u=%u", getuid ());
				}
#endif
				/* use the same kind of workaround for the "locale=" option (used by ntfs-3g) */
				if (strcmp (option, "locale=") == 0) {
					g_free (option);
					option = g_strdup_printf ("locale=%s", setlocale (LC_ALL, ""));
				}
				g_ptr_array_add (mount_options, option);

				g_slist_free_1 (l);
			}
		}
		g_free (key);
	}


	
	/* If we don't have a mount point yet... Use the label of the file system if available */
	if (mount_point == NULL) {
		if (volume != NULL) {
			const char *label;
			label = libhal_volume_get_label (volume);
			if (label != NULL) {
				int n;

				mount_point = g_strdup (label);

				/* However, the label may contain G_DIR_SEPARATOR so just replace these
				 * with underscores. Pretty typical use-case, suppose you hotplug a disk
				 * from a server, then you get e.g. two ext3 fs'es with labels '/' and
				 * '/home' - typically seen on Red Hat systems... 
				 */
				for (n = 0; mount_point[n] != '\0'; n++) {
					if (mount_point[n] == G_DIR_SEPARATOR) {
						mount_point[n] = '_';
					}
				}
			}
		}
		
		/* for testing:
		 * mount_point = g_strdup ("foobar der æøå サイトの"); */
	}
	
	
	
	ret = volume_mount_with_options (udi, volume, drive, mount_point, fstype_override, mount_options);
	
	g_ptr_array_free (mount_options, TRUE);
	g_free (mount_point);
	g_free (fstype_override);

out:
	return ret;
}

static guint unmount_cache_timeout_id = -1;

#ifdef ENABLE_NOTIFY
static NotifyNotification *unmount_note = NULL;
#endif

static gboolean unmount_note_is_eject = FALSE;
static char *unmount_note_drive_name = NULL;
static char *unmount_note_icon_name = NULL;

#ifdef ENABLE_NOTIFY
static void
unmount_note_close_func (NotifyNotification *note, gpointer user_data)
{
       g_debug ("in unmount_note_close_func()");
       unmount_note = NULL;
}
#endif

static gboolean
unmount_cache_timeout_func (gpointer data)
{
#ifdef ENABLE_NOTIFY
	if (!opt_noui) {
		char *summary;
		char *message;

		g_debug ("putting up Flushing Cache notification");

		summary = _("Writing data to device");
		message = g_strdup_printf (_("There is data that needs to be written to the device %s before it can be removed. Please do not remove the media or disconnect the drive."), unmount_note_drive_name);
		unmount_note = notify_notification_new (summary,
							message,
							unmount_note_icon_name);
		if (unmount_note == NULL) {
			g_warning ("Cannot create note for unmount cache sync");
		} else {
			notify_notification_set_timeout (unmount_note, NOTIFY_EXPIRES_NEVER);
			g_signal_connect (unmount_note, "closed", G_CALLBACK (unmount_note_close_func), NULL);
			notify_notification_show (unmount_note, NULL);

		}
		g_free (message);
	}
#endif
	
	return FALSE;
}

static void
unmount_cache_timeout_start (char *drive_name, char *icon_name, gboolean is_eject)
{
	unmount_note_is_eject = is_eject;
	unmount_note_drive_name = g_strdup (drive_name);
	if (icon_name)
		unmount_note_icon_name = g_strdup (icon_name);
	else
		unmount_note_icon_name = g_strdup ("drive-harddisk");
	g_strchug (unmount_note_drive_name);
	g_debug("Icon %s\n", unmount_note_icon_name);
	g_debug ("Setting up 750ms timer for Flushing Cache dialog");
	unmount_cache_timeout_id = g_timeout_add (750, unmount_cache_timeout_func, NULL);
#ifdef ENABLE_NOTIFY
	unmount_note = NULL;
#endif
}

static void
unmount_cache_timeout_cancel (gboolean was_success)
{
	g_source_remove (unmount_cache_timeout_id);
#ifdef ENABLE_NOTIFY
	if (unmount_note != NULL) {
		if (was_success) {
			char *summary;
			char *message;
			
			g_debug ("Updating Flushing Cache notification");
			
			summary = _("Device is now safe to remove");
			message = g_strdup_printf (_("The device %s is now safe to remove."), 
						   unmount_note_drive_name);
			notify_notification_close (unmount_note, NULL);
			notify_notification_update (unmount_note, 
						    summary, 
						    message, 
						    unmount_note_icon_name);
			g_free (message);
			notify_notification_set_timeout (unmount_note, NOTIFY_EXPIRES_DEFAULT);
			notify_notification_show (unmount_note, NULL);
		}
	}
#endif
}

static gboolean unmount_still_in_progress = FALSE;

static DBusMessage *unmount_reply = NULL;

static void
unmount_done (DBusPendingCall *pending_call,
	      void            *user_data)
{
	g_debug ("in unmount_done : user_data = 0x%x", user_data);

	unmount_reply = dbus_pending_call_steal_reply (pending_call);
	unmount_still_in_progress = FALSE;
	dbus_pending_call_unref (pending_call);
}

static char *
volume_icon(const char *udi)
{
	GVolumeMonitor *monitor;
	GList *volumes, *tmp;

	char *vol_udi;
	GVolume *volume;
	GMount *mount;
	GIcon *icon;
	const char * const * names;

	char *icon_name;

	g_assert (udi);

	monitor = g_volume_monitor_get ();
	if (!monitor)
		return NULL;

	icon_name = NULL;

	volumes = g_volume_monitor_get_volumes (monitor);

	/* Find it... */
	for (tmp = volumes; tmp != NULL; tmp = tmp->next) {
		volume = tmp->data;

		vol_udi = g_volume_get_identifier (volume,
						   G_VOLUME_IDENTIFIER_KIND_HAL_UDI);
		if (vol_udi) {
			if (strcmp (udi, vol_udi) == 0) {
				g_free (vol_udi);
				break;
			}
			g_free (vol_udi);
		}

		g_object_unref (volume);
	}

	/* Figure out its icon... */
	if (tmp) {
		mount = g_volume_get_mount (volume);
		if (mount) {
			icon = g_mount_get_icon (mount);
			g_object_unref (mount);
		} else {
			icon = g_volume_get_icon (volume);
		}

		g_object_unref (volume);

		if (icon) {
			if (G_IS_THEMED_ICON (icon)) {
				names = g_themed_icon_get_names (G_THEMED_ICON(icon));
				if (names[0])
					icon_name = g_strdup (names[0]);
			}
			g_object_unref (icon);
		}
	}

	/* Cleanup... */
	for (; tmp != NULL; tmp = tmp->next) {
		volume = tmp->data;
		g_object_unref (volume);
	}

	g_list_free (volumes);

	g_object_unref (monitor);

	return icon_name;
}

static gboolean
volume_unmount (const char *udi, LibHalVolume *volume, LibHalDrive *drive)
{
	gboolean ret;
	DBusMessage *msg = NULL;
	DBusMessage *reply = NULL;
	DBusError error;
	char **options = NULL;
	unsigned int num_options = 0;
	const char *fstype;
	const char *device_file;
	const char *label;
	const char *uuid;
	char *icon_name;
	char *model;
	char *vendor;
	char *drive_name;
	DBusPendingCall *pending_return;
	gboolean is_ro;

	ret = FALSE;

	g_debug ("Unmounting %s", udi);

	is_ro = FALSE;

	model = NULL;
	vendor = NULL;
	if (drive != NULL) {
		model = g_strdup (libhal_drive_get_model (drive));
		vendor = g_strdup (libhal_drive_get_vendor (drive));
		if (libhal_drive_get_type (drive) == LIBHAL_DRIVE_TYPE_CDROM) {
			is_ro = TRUE;
		}
	} else if (volume != NULL) {
		drive = libhal_drive_from_udi (hal_ctx, 
					       libhal_volume_get_storage_device_udi (volume));
		if (drive != NULL) {
			model = g_strdup (libhal_drive_get_model (drive));
			vendor = g_strdup (libhal_drive_get_vendor (drive));
			if (libhal_drive_get_type (drive) == LIBHAL_DRIVE_TYPE_CDROM) {
				is_ro = TRUE;
			}
			libhal_drive_free (drive);
			drive = NULL;
		}

		if (!is_ro) {
			is_ro = libhal_volume_is_mounted_read_only (volume);
		}
	}

	icon_name = volume_icon (udi);

	if (vendor != NULL && model != NULL)
		drive_name = g_strdup_printf (" %s %s", vendor, model);
	else if (vendor != NULL)
		drive_name = g_strdup_printf (" %s", vendor);
	else if (model != NULL)
		drive_name = g_strdup_printf (" %s", model);
	else
		drive_name = g_strdup ("");

	/* check if it's in /etc/fstab */
	label = NULL;
	uuid = NULL;
	device_file = NULL;
	if (volume != NULL) {
		label = libhal_volume_get_label (volume);
		uuid = libhal_volume_get_uuid (volume);
		device_file = libhal_volume_get_device_file (volume);
	} else if (drive != NULL) {
		device_file = libhal_drive_get_device_file (drive);
	}
	if (device_file != NULL) {
		char *mount_point = NULL;

		if (is_in_fstab (device_file, label, uuid, &mount_point)) {
			GError *err = NULL;
			char *sout = NULL;
			char *serr = NULL;
			int exit_status;
			char *args[3] = {UMOUNT, NULL, NULL};
			char **envp = {NULL};

			/* don't unmount if device is locked
                         */
                        if (libhal_device_is_locked_by_others (hal_ctx,
                                                               udi,
                                                               "org.freedesktop.Hal.Device.Storage",
                                                               NULL)) {
                                if (opt_verbose)
                                        g_message (_("Device %s is locked, aborting"),
                                                   udi);
                                goto out;
                        }
                        if (opt_verbose)
                                g_print (_("Device %s is in /etc/fstab with mount point \"%s\"\n"),
                                         device_file, mount_point);
			args[1] = mount_point;
			if (!g_spawn_sync ("/",
					   args,
					   envp,
					   0,
					   NULL,
					   NULL,
					   &sout,
					   &serr,
					   &exit_status,
					   &err)) {
				g_warning ("Cannot execute %s\n", UMOUNT);
				g_free (mount_point);
				goto out;
			}

			if (exit_status != 0) {
                                if (opt_verbose)
                                        g_message ("%s said error %d, stdout='%s', stderr='%s'\n", 
                                                   UMOUNT, exit_status, sout, serr);
				g_free (mount_point);

				if (strstr (serr, "is busy") != NULL) {
					show_error_dialog_unmount (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.Busy", serr);
				} else if (strstr (serr, "not mounted") != NULL) {
					show_error_dialog_unmount (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.NotMounted", serr);
				} else if (strstr (serr, "only root") != NULL) {
					show_error_dialog_unmount (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.PermissionDenied", serr);
				} else {
					show_error_dialog_unmount (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.UnknownFailure", serr);
				}

				goto out;
			}

                        if (opt_verbose)
                                g_print (_("Unmounted %s (using /etc/fstab)\n"), device_file);

			g_free (mount_point);
			ret = TRUE;
			goto out;

		}
		g_free (mount_point);
	}


	msg = dbus_message_new_method_call ("org.freedesktop.Hal", udi,
					    "org.freedesktop.Hal.Device.Volume",
					    "Unmount");
	if (msg == NULL) {
		g_warning ("Could not create dbus message for %s", udi);
		goto out;
	}

	if (!dbus_message_append_args (msg, 
				       DBUS_TYPE_ARRAY, DBUS_TYPE_STRING, &options, num_options,
				       DBUS_TYPE_INVALID)) {
		g_warning ("Could not append args to dbus message for %s", udi);
		goto out;
	}

	/* if Unmount() takes a long time.. and it might if we're flushing a lot of
	 * data to disk, see 
	 *
	 *  https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=194296
	 *
	 * So put up a dialog after some time that tells the user we're flushing
	 * the cache.
	 */
	if (!is_ro) {
		unmount_cache_timeout_start (drive_name, icon_name, FALSE);
	}

	/* it also means we can't use dbus_connection_send_with_reply_and_block() as
	 * that function doesn't enter the mainloop
	 */
	if (!dbus_connection_send_with_reply (dbus_connection, msg, &pending_return, DBUS_TIMEOUT)) {
		/* TODO: what error dialog to show for OOM? */
		goto out;
	}

	dbus_pending_call_set_notify (pending_return,
				      unmount_done,
				      NULL,
				      NULL);
	unmount_still_in_progress = TRUE;

	/* run the main loop */
	while (unmount_still_in_progress) {
		g_main_context_iteration (NULL, TRUE);
	}

	reply = unmount_reply;

	dbus_error_init (&error);
	if (dbus_set_error_from_message (&error, reply)) {
		if (!is_ro) {
			unmount_cache_timeout_cancel (FALSE);
		}
                if (opt_verbose)
                        g_message ("Unmount failed for %s: %s : %s\n", udi, error.name, error.message);

		if (strcmp (error.name, "org.freedesktop.Hal.Device.PermissionDeniedByPolicy") == 0) {
                        /* attempt to gain privilege */
                        if (attempt_to_gain_privilege (error.message)) {
                                ret = volume_unmount (udi, volume, drive);
                        }
                        dbus_error_free (&error);
                        return ret;
                }

		show_error_dialog_unmount (udi, volume, drive, error.name, error.message);
		dbus_error_free (&error);
		goto out;
	}

        if (opt_verbose)
                g_print (_("Unmounted %s\n"), get_dev_file (volume, drive));
	if (!is_ro) {
		unmount_cache_timeout_cancel (TRUE);
	}

	ret = TRUE;
out:
	if (msg != NULL)
		dbus_message_unref (msg);
	if (reply != NULL)
		dbus_message_unref (reply);
	if (icon_name != NULL)
		g_free (icon_name);

	g_free (drive_name);

	return ret;
}

static gboolean
volume_eject (const char *udi, LibHalVolume *volume, LibHalDrive *drive)
{
	gboolean ret = FALSE;
	DBusMessage *msg = NULL;
	DBusMessage *reply = NULL;
	DBusError error;
	char **options = NULL;
	unsigned int num_options = 0;
	const char *fstype;
	const char *device_file;
	const char *label;
	const char *uuid;
	char *icon_name;
	char *model;
	char *vendor;
	char *drive_name;
	gboolean is_ro;
	DBusPendingCall *pending_return;

	ret = FALSE;

	g_debug ("Ejecting %s", udi);

	is_ro = FALSE;

	model = NULL;
	vendor = NULL;
	if (drive != NULL) {
		model = g_strdup (libhal_drive_get_model (drive));
		vendor = g_strdup (libhal_drive_get_vendor (drive));
		if (libhal_drive_get_type (drive) == LIBHAL_DRIVE_TYPE_CDROM) {
			is_ro = TRUE;
		}
	} else if (volume != NULL) {
		drive = libhal_drive_from_udi (hal_ctx, 
					       libhal_volume_get_storage_device_udi (volume));
		if (drive != NULL) {
			model = g_strdup (libhal_drive_get_model (drive));
			vendor = g_strdup (libhal_drive_get_vendor (drive));
			if (libhal_drive_get_type (drive) == LIBHAL_DRIVE_TYPE_CDROM) {
				is_ro = TRUE;
			}
			libhal_drive_free (drive);
			drive = NULL;
		}

		if (!is_ro) {
			is_ro = libhal_volume_is_mounted_read_only (volume);
		}
	}

	icon_name = volume_icon (udi);

	if (vendor != NULL && model != NULL)
		drive_name = g_strdup_printf (" %s %s", vendor, model);
	else if (vendor != NULL)
		drive_name = g_strdup_printf (" %s", vendor);
	else if (model != NULL)
		drive_name = g_strdup_printf (" %s", model);
	else
		drive_name = g_strdup ("");

	/* check if it's in /etc/fstab */
	label = NULL;
	uuid = NULL;
	device_file = NULL;
	if (volume != NULL) {
		label = libhal_volume_get_label (volume);
		uuid = libhal_volume_get_uuid (volume);
		device_file = libhal_volume_get_device_file (volume);
	} else if (drive != NULL) {
		device_file = libhal_drive_get_device_file (drive);
	}
	if (device_file != NULL) {
		char *mount_point = NULL;

		if (is_in_fstab (device_file, label, uuid, &mount_point)) {
			GError *err = NULL;
			char *sout = NULL;
			char *serr = NULL;
			int exit_status;
			char *args[3] = {"eject", NULL, NULL};
			char *envp[3] = {"LC_ALL=C", "LANG=C", NULL};

			/* don't mount if device is locked
                         */
                        if (libhal_device_is_locked_by_others (hal_ctx,
                                                               udi,
                                                               "org.freedesktop.Hal.Device.Storage",
                                                               NULL)) {
                                if (opt_verbose)
                                        g_message (_("Device %s is locked, aborting"),
                                                   udi);
                                goto out;
                        }

                        if (opt_verbose)
                                g_print (_("Device %s is in /etc/fstab with mount point \"%s\"\n"), 
                                         device_file, mount_point);
			args[1] = mount_point;
			if (!g_spawn_sync ("/",
					   args,
					   envp,
					   G_SPAWN_SEARCH_PATH,
					   NULL,
					   NULL,
					   &sout,
					   &serr,
					   &exit_status,
					   &err)) {
				g_warning ("Cannot execute %s\n", "eject");
				g_free (mount_point);
				goto out;
			}

			if (exit_status != 0) {
                                if (opt_verbose)
                                        g_message ("%s said error %d, stdout='%s', stderr='%s'\n", 
                                                   "eject", exit_status, sout, serr);
				g_free (mount_point);

				if (strstr (serr, "is busy") != NULL) {
					show_error_dialog_eject (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.Busy", serr);
				} else if (strstr (serr, "only root") != NULL) {
					show_error_dialog_eject (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.PermissionDenied", serr);
				} else if (strstr (serr, "unable to open") != NULL) {
					show_error_dialog_eject (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.PermissionDenied", serr);
				} else {
					show_error_dialog_eject (udi, volume, drive, "org.freedesktop.Hal.Device.Volume.UnknownFailure", serr);
				}

				goto out;
			}

                        if (opt_verbose)
                                g_print (_("Ejected %s (using /etc/fstab).\n"), device_file);

			g_free (mount_point);
			ret = TRUE;
			goto out;

		}
		g_free (mount_point);
	}
	
	msg = dbus_message_new_method_call ("org.freedesktop.Hal", udi,
					    "org.freedesktop.Hal.Device.Volume",
					    "Eject");
	if (msg == NULL) {
		g_warning ("Could not create dbus message for %s", udi);
		goto out;
	}

	if (!dbus_message_append_args (msg, 
				       DBUS_TYPE_ARRAY, DBUS_TYPE_STRING, &options, num_options,
				       DBUS_TYPE_INVALID)) {
		g_warning ("Could not append args to dbus message for %s", udi);
		goto out;
	}

	/* if Eject() takes a long time.. and it might if we're flushing a lot of
	 * data to disk, see 
	 *
	 *  https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=194296
	 *
	 * So put up a dialog after some time that tells the user we're flushing
	 * the cache.
	 */
	if (!is_ro) {
		unmount_cache_timeout_start (drive_name, icon_name, TRUE);
	}

	/* it also means we can't use dbus_connection_send_with_reply_and_block() as
	 * that function doesn't enter the mainloop
	 */
	if (!dbus_connection_send_with_reply (dbus_connection, msg, &pending_return, DBUS_TIMEOUT)) {
		/* TODO: what error dialog to show for OOM? */
		goto out;
	}

	dbus_pending_call_set_notify (pending_return,
				      unmount_done,
				      NULL,
				      NULL);
	unmount_still_in_progress = TRUE;

	/* run the main loop */
	while (unmount_still_in_progress) {
		g_main_context_iteration (NULL, TRUE);
	}

	reply = unmount_reply;

	dbus_error_init (&error);
	if (dbus_set_error_from_message (&error, reply)) {
		if (!is_ro) {
			unmount_cache_timeout_cancel (FALSE);
		}
                if (opt_verbose)
                        g_message ("Eject failed for %s: %s : %s\n", udi, error.name, error.message);

		if (strcmp (error.name, "org.freedesktop.Hal.Device.PermissionDeniedByPolicy") == 0) {
                        /* attempt to gain privilege */
                        if (attempt_to_gain_privilege (error.message)) {
                                ret = volume_eject (udi, volume, drive);
                        }
                        dbus_error_free (&error);
                        return ret;
                }

		show_error_dialog_eject (udi, volume, drive, error.name, error.message);
		dbus_error_free (&error);
		goto out;
	}

        if (opt_verbose)
                g_print (_("Ejected %s\n"), get_dev_file (volume, drive));
	if (!is_ro) {
		unmount_cache_timeout_cancel (TRUE);
	}

	ret = TRUE;

out:
	if (msg != NULL)
		dbus_message_unref (msg);
	if (reply != NULL)
		dbus_message_unref (reply);
	if (icon_name != NULL)
		g_free (icon_name);

	g_free (drive_name);

	return ret;
}


static char *
lookup_password (const char *udi, gboolean *is_session)
{
	char *password;
	GList *keyring_result;

	password = NULL;

	if (gnome_keyring_find_network_password_sync (g_get_user_name (),     /* user */
						      NULL,                   /* domain */
						      udi,                    /* server */
						      "password",             /* object */
						      "org.gnome.Mount",      /* protocol */
						      NULL,                   /* authtype */
						      0,                      /* port */
						      &keyring_result) != GNOME_KEYRING_RESULT_OK)
		return FALSE;
	
	if (keyring_result != NULL && g_list_length (keyring_result) == 1) {
		GnomeKeyringNetworkPasswordData *data1 = keyring_result->data;

		if (strcmp (data1->object, "password") == 0) {
			password = g_strdup (data1->password);
		}

		if (password != NULL) {
			if (strcmp (data1->keyring, "session") == 0)
				*is_session = TRUE;
			else
				*is_session = FALSE;
		}

		gnome_keyring_network_password_list_free (keyring_result);
	}

	return password;
}

static void 
save_password (const char *udi, const char *keyring, const char *password)
{
	guint32 item_id;
	GnomeKeyringResult keyring_result;

	keyring_result = gnome_keyring_set_network_password_sync (keyring,            /* keyring  */
								  g_get_user_name (), /* user     */
								  NULL,               /* domain   */
								  udi,                /* server   */
								  "password",         /* object   */
								  "org.gnome.Mount",  /* prtocol  */
								  NULL,               /* authtype */
								  0,                  /* port     */
								  password,           /* password */
								  &item_id);          /* item_id  */
	if (keyring_result != GNOME_KEYRING_RESULT_OK)
	{
		g_warning ("Couldn't store password in keyring, code %d", (int) keyring_result);
	}
}


static char *
get_password (const char *udi, LibHalVolume *volume, LibHalDrive *drive, gboolean retry)
{
	char            *drive_name;
	char            *result;
	char            *prompt;
	GtkWidget	*dialog;
	GnomePasswordDialogRemember remember;
	char            *keyring_password;
	gboolean         keyring_is_session;
	LibHalDrive     *drv;
	char            *model;
	char            *vendor;
        char            *title;

	keyring_password = NULL;
	keyring_is_session = FALSE;

	result = NULL;

	model = NULL;
	vendor = NULL;
	if (drive != NULL) {
		model = g_strdup (libhal_drive_get_model (drive));
		vendor = g_strdup (libhal_drive_get_vendor (drive));
	} else if (volume != NULL) {
		drive = libhal_drive_from_udi (hal_ctx, 
					       libhal_volume_get_storage_device_udi (volume));
		if (drive != NULL) {
			model = g_strdup (libhal_drive_get_model (drive));
			vendor = g_strdup (libhal_drive_get_vendor (drive));
			libhal_drive_free (drive);
			drive = NULL;
		}
	}

	if (vendor != NULL && model != NULL)
		drive_name = g_strdup_printf (" %s %s", vendor, model);
	else if (vendor != NULL)
		drive_name = g_strdup_printf (" %s", vendor);
	else if (model != NULL)
		drive_name = g_strdup_printf (" %s", model);
	else
		drive_name = g_strdup ("");
		
	keyring_password = lookup_password (udi, &keyring_is_session);

	if (!retry)
	{
		if (keyring_password != NULL) {
			result = g_strdup (keyring_password);
			goto out;
		}
	}

	/* ask on console if we don't have a display */
	if (opt_nodisplay) {
		char *password;
		char *prompt;

		prompt = g_strdup_printf (_("Enter password to unlock encrypted data for %s: "), 
					  get_dev_file (volume, drive));

		password = getpass (prompt);
		if (password != NULL)
			result = g_strdup (password);
		goto out;
	}

	if (opt_noui) {
                if (opt_verbose)
                        g_message ("Not showing password dialog since invoked with --no-ui");
		goto out;
	}

	/* We don't really want to block - tell parent it's going to
	 * be alright even though we don't know yet.. 
	 *
	 * If we don't do this Nautilus will put up a "press cancel"
	 * dialog when we ask for the password..
	 */
	notify_parent (TRUE);

        if (libhal_volume_is_partition (volume)) {
                prompt = g_strdup_printf (_("The storage device %s contains encrypted data on partition %d. "
                                            "Enter a password to unlock."),
                                          drive_name,
                                          libhal_volume_get_partition_number (volume));
                title = g_strdup_printf (_("Unlock Encrypted Data (partition %d)"),
                                         libhal_volume_get_partition_number (volume));
        } else {
                prompt = g_strdup_printf (_("The storage device %s contains encrypted data. "
                                            "Enter a password to unlock."),
                                          drive_name);
                title = g_strdup_printf (_("Unlock Encrypted Data"));
        }

	dialog = gnome_password_dialog_new (title, prompt, NULL, NULL, FALSE);
	g_free (prompt);
        g_free (title);

	gnome_password_dialog_set_show_username (GNOME_PASSWORD_DIALOG (dialog), FALSE);
	gnome_password_dialog_set_show_userpass_buttons (GNOME_PASSWORD_DIALOG (dialog), FALSE);
	gnome_password_dialog_set_show_domain (GNOME_PASSWORD_DIALOG (dialog), FALSE);
	gnome_password_dialog_set_show_remember (GNOME_PASSWORD_DIALOG (dialog), TRUE);
	/* use the same keyring storage options as from the items we put in the entry boxes */
	remember = GNOME_PASSWORD_DIALOG_REMEMBER_NOTHING;

	/* use the same keyring storage options as from the items we put in the entry boxes */
	remember = GNOME_PASSWORD_DIALOG_REMEMBER_NOTHING;
	if (keyring_password != NULL) {
		if (keyring_is_session)
			remember = GNOME_PASSWORD_DIALOG_REMEMBER_SESSION;
		else
			remember = GNOME_PASSWORD_DIALOG_REMEMBER_FOREVER;				
	}

	gnome_password_dialog_set_remember (GNOME_PASSWORD_DIALOG (dialog), remember);

	/* if retrying, put in the passwords from the keyring */
	if (keyring_password != NULL) {
		gnome_password_dialog_set_password (GNOME_PASSWORD_DIALOG (dialog), keyring_password);
	}

	gtk_window_present (GTK_WINDOW (dialog));

	if (gnome_password_dialog_run_and_block (GNOME_PASSWORD_DIALOG (dialog)))
	{
		char *password;

		password = gnome_password_dialog_get_password (GNOME_PASSWORD_DIALOG (dialog));
		result = password;

		switch (gnome_password_dialog_get_remember (GNOME_PASSWORD_DIALOG (dialog)))
		{
			case GNOME_PASSWORD_DIALOG_REMEMBER_SESSION:
				save_password (udi, "session", password);
				break;
			case GNOME_PASSWORD_DIALOG_REMEMBER_FOREVER:
				save_password (udi, NULL, password);
				break;
			default:
				break;
		}

	}

	gtk_widget_destroy (dialog);

out:
	g_free (keyring_password);
	g_free (drive_name);
	return result;
}

static gboolean
setup_crypto (const char *udi, LibHalVolume *volume, LibHalDrive *drive, 
 	      const char *password, gboolean *password_error)
{
	gboolean ret = FALSE;
	DBusMessage *msg = NULL;
	DBusMessage *reply = NULL;
	DBusError error;

	*password_error = FALSE;

	g_debug ("Setting up %s for crypto", udi);
	
	msg = dbus_message_new_method_call ("org.freedesktop.Hal", udi,
					    "org.freedesktop.Hal.Device.Volume.Crypto",
					    "Setup");
	if (msg == NULL) {
		g_warning ("Could not create dbus message for %s", udi);
		goto out;
	}

	if (!dbus_message_append_args (msg, 
				       DBUS_TYPE_STRING, &password,
				       DBUS_TYPE_INVALID)) {
		g_warning ("Could not append args to dbus message for %s", udi);
		goto out;
	}
	
	dbus_error_init (&error);
	if (!(reply = dbus_connection_send_with_reply_and_block (dbus_connection, msg, DBUS_TIMEOUT, &error)) || 
	    dbus_error_is_set (&error)) {
                if (opt_verbose)
                        g_message ("Setup failed for %s: %s : %s\n", udi, error.name, error.message);
		if (strcmp (error.name, "org.freedesktop.Hal.Device.Volume.Crypto.SetupPasswordError") == 0) {
			*password_error = TRUE;
		}
		dbus_error_free (&error);
		goto out;
	}


	ret = TRUE;

        if (opt_verbose)
                g_print (_("Setup clear-text device for %s.\n"), get_dev_file (volume, drive));

out:
	if (msg != NULL)
		dbus_message_unref (msg);
	if (reply != NULL)
		dbus_message_unref (reply);

	return ret;
}

static gboolean
teardown_crypto (const char *udi, LibHalVolume *volume, LibHalDrive *drive)
{
	gboolean ret = FALSE;
	DBusMessage *msg = NULL;
	DBusMessage *reply = NULL;
	DBusError error;
	char *clear_udi;

	g_debug ("Tearing down %s for crypto", udi);
	
	clear_udi = libhal_volume_crypto_get_clear_volume_udi (hal_ctx, volume);
	if (clear_udi != NULL) {
		LibHalVolume *clear_volume;
		gboolean unmount_child_failed;

		unmount_child_failed = FALSE;

		clear_volume = libhal_volume_from_udi (hal_ctx, clear_udi);
		if (clear_volume != NULL) {
			if (libhal_volume_is_mounted (clear_volume)) {
				if (!volume_unmount (clear_udi, clear_volume, NULL))
					unmount_child_failed = TRUE;
			}
			libhal_volume_free (clear_volume);
		}
		free (clear_udi);

		if (unmount_child_failed)
			goto out;
	}


	msg = dbus_message_new_method_call ("org.freedesktop.Hal", udi,
					    "org.freedesktop.Hal.Device.Volume.Crypto",
					    "Teardown");
	if (msg == NULL) {
		g_warning ("Could not create dbus message for %s", udi);
		goto out;
	}

	if (!dbus_message_append_args (msg, 
				       DBUS_TYPE_INVALID)) {
		g_warning ("Could not append args to dbus message for %s", udi);
		goto out;
	}
	
	dbus_error_init (&error);
	if (!(reply = dbus_connection_send_with_reply_and_block (dbus_connection, msg, DBUS_TIMEOUT, &error)) || 
	    dbus_error_is_set (&error)) {
		g_warning ("Teardown failed for %s: %s : %s\n", udi, error.name, error.message);
		dbus_error_free (&error);
		goto out;
	}

        if (opt_verbose)
                g_print (_("Teared down clear-text device for %s.\n"), get_dev_file (volume, drive));

	ret = TRUE;

out:
	if (msg != NULL)
		dbus_message_unref (msg);
	if (reply != NULL)
		dbus_message_unref (reply);

	return ret;
}

static const char *crypto_setup_backing_udi = NULL;
static int crypto_setup_rc = 1;

static void
crypto_setup_device_removed (LibHalContext *ctx, const char *udi)
{
	g_debug ("In crypto_setup_device_removed for %s", udi);

	if (strcmp (udi, crypto_setup_backing_udi) == 0) {
		g_debug ("Backing volume removed! Exiting..");
		gtk_exit (1);		
	}
}

static void
crypto_setup_device_added (LibHalContext *ctx, const char *udi)
{
	const char *backing_udi;
	DBusError error;
	LibHalVolume *volume;

	g_debug ("In crypto_setup_device_added for %s", udi);

	backing_udi = NULL;
	volume = libhal_volume_from_udi (hal_ctx, udi);
	if (volume != NULL) {
		backing_udi = libhal_volume_crypto_get_backing_volume_udi (volume);
		if (backing_udi != NULL) {
			if (strcmp (backing_udi, crypto_setup_backing_udi) == 0) {
				LibHalVolumeUsage fsuage;

				g_debug ("%s is backed by %s - will mount", udi, backing_udi);

                                if (opt_verbose)
                                        g_print (_("Clear text device is %s. Mounting.\n"), get_dev_file (volume, NULL));
					
				fsuage = libhal_volume_get_fsusage (volume);
					
				if (fsuage == LIBHAL_VOLUME_USAGE_MOUNTABLE_FILESYSTEM) {
					if (volume_mount (udi, volume, NULL))
						crypto_setup_rc = 0;
				} else {
                                        if (opt_verbose)
                                                g_message ("%s does not have a mountable filesystem", udi);
				}
				
				notify_parent (crypto_setup_rc == 0);
				gtk_main_quit ();				
			}
		}
		
		libhal_volume_free (volume);
	}

}

static gboolean
crypto_setup_timeout (gpointer data)
{
        if (opt_verbose)
                g_message ("Timeout for waiting for cleartext device... Exiting.");
	notify_parent (FALSE);
	gtk_main_quit ();
	
	return FALSE;
}


static void 
my_empty_log_handler (const gchar *log_domain,
		      GLogLevelFlags log_level,
		      const gchar *message,
		      gpointer user_data)
{
}

static void
erase_settings (LibHalVolume *volume, LibHalDrive *drive)
{
	char *key;
	char *keydir;

	if (volume != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_volume_get_udi (volume));
		g_print ("Erasing settings for volume\nhal udi:      %s\n", udi2);
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/volumes/%s", udi2);
		g_free (udi2);
	} else if (drive != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_drive_get_udi (drive));
		g_print ("Erasing settings for drive\nhal udi:      %s\n", udi2);
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/drives/%s", udi2);
		g_free (udi2);
	} else {
		goto out;
	}
	g_debug ("Erasing from gconf directory %s", keydir);

	key = g_strdup_printf ("%s/mount_point", keydir);
	gconf_client_unset (gconf_client, key, NULL);
	g_free (key);

	key = g_strdup_printf ("%s/mount_options", keydir);
	gconf_client_unset (gconf_client, key, NULL);
	g_free (key);
	
	key = g_strdup_printf ("%s/fstype_override", keydir);
	gconf_client_unset (gconf_client, key, NULL);
	g_free (key);
	
	g_free (keydir);
out:
	;
}

static void
write_settings (LibHalVolume *volume, LibHalDrive *drive)
{
	char *keydir;
	char *key;
	gboolean set_something = FALSE;

	if (volume != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_volume_get_udi (volume));
		g_print ("Writing settings for volume\nhal udi:      %s\n", udi2);
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/volumes/%s", udi2);
		g_free (udi2);
	} else if (drive != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_drive_get_udi (drive));
		g_print ("Writing settings for drive\nhal udi:      %s\n", udi2);
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/drives/%s", udi2);
		g_free (udi2);
	} else {
		goto out;
	}
	g_debug ("Writing to gconf directory %s", keydir);
		
	key = g_strdup_printf ("%s/mount_point", keydir);
	if (mount_point_from_command_line != NULL) {
		gconf_client_set_string (gconf_client, key, mount_point_from_command_line, NULL);
		set_something = TRUE;
	}
	g_free (key);
	
	key = g_strdup_printf ("%s/mount_options", keydir);
	if (mount_options_from_command_line != NULL) {
		int i;
		char **opt_from_cmd_line;
		GSList *l;
		
		l = NULL;
		opt_from_cmd_line = g_strsplit (mount_options_from_command_line, ",", 0);
		for (i = 0; opt_from_cmd_line[i] != NULL; i++) {
			l = g_slist_append (l, opt_from_cmd_line[i]);
			set_something = TRUE;
		}
		
		gconf_client_set_list (gconf_client, key, GCONF_VALUE_STRING, l, NULL);
		
		g_strfreev (opt_from_cmd_line);
		g_slist_free (l);
	}
	g_free (key);
	
	key = g_strdup_printf ("%s/fstype_override", keydir);
	if (fstype_from_command_line != NULL) {
		gconf_client_set_string (gconf_client, key, fstype_from_command_line, NULL);
		set_something = TRUE;
	}
	g_free (key);
	
	if (!set_something) {
		g_print ("Use --mount-point, --mount-options and --fstype to specify options\n");
	}
	
	g_free (keydir);
out:
	;
}

static void
display_settings (LibHalVolume *volume, LibHalDrive *drive)
{
	GSList *options_list;
	GSList *l;
	GSList *next;
	char *keydir;
	char *key;
	char *mount_point;
	char *fstype_override;
	gboolean printed_something = FALSE;

	if (volume != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_volume_get_udi (volume));
		g_print ("Displaying settings for volume (overrides drive settings)\nhal udi:      %s\n", udi2);
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/volumes/%s", udi2);
		g_free (udi2);
	} else if (drive != NULL) {
		int n;
		char *udi2;
				
		udi2 = g_strdup (libhal_drive_get_udi (drive));
		g_print ("Displaying settings for drive (affects all volumes, unless overridden)\nhal udi:      %s\n", udi2);
		for (n = 0; udi2[n] != '\0'; n++) {
			if (udi2[n] == '/')
				udi2[n] = '_';
		}
		keydir = g_strdup_printf ("/system/storage/drives/%s", udi2);
		g_free (udi2);
	} else {
		goto out;
	}
	g_debug ("Reading from gconf directory %s", keydir);
		
	key = g_strdup_printf ("%s/mount_point", keydir);
	mount_point = gconf_client_get_string (gconf_client, key, NULL);
	if (mount_point != NULL) {
		g_print ("mount point:  %s\n", mount_point);
		printed_something = TRUE;
	}
	g_free (mount_point);
	g_free (key);
		
	key = g_strdup_printf ("%s/mount_options", keydir);
	if ((options_list = gconf_client_get_list (gconf_client, key, GCONF_VALUE_STRING, NULL)) != NULL) {
		for (l = options_list; l != NULL; l = next) {
			char *option;
			
			next = l->next;
			option = l->data;
			
			g_print ("mount option: %s\n", option);
			printed_something = TRUE;
			
			g_slist_free_1 (l);
		}
	}
	g_free (key);
	
	key = g_strdup_printf ("%s/fstype_override", keydir);
	fstype_override = gconf_client_get_string (gconf_client, key, NULL);
	if (fstype_override != NULL) {
		g_print ("fs type:      %s\n", fstype_override);
		printed_something = TRUE;
	}
	g_free (fstype_override);
	g_free (key);

	if (!printed_something) {
		g_print ("There are no settings; you can use --write-settings\n");
	}
		
	g_free (keydir);
out:
	;
}

int
main (int argc, char *argv[])
{
	pid_t pid;
	char buf;
	int dev_null_fd;
	LibHalDrive *drive = NULL;
	LibHalVolume *volume = NULL;
	const char *udi;
	char *resolved_device_file = NULL;
	static gboolean opt_connect_crypto = FALSE;
	static gboolean opt_disconnect_crypto = FALSE;
	static gboolean opt_unmount = FALSE;
	static gboolean opt_eject = FALSE;
	static gchar *opt_hal_udi = NULL;
	static gchar *opt_device_file = NULL;
	static gchar *opt_nickname = NULL;
	static gchar *opt_mount_point = NULL;
	static gchar *opt_mount_options = NULL;
	static gchar *opt_extra_mount_options = NULL;
	static gchar *opt_fstype = NULL;
	static gboolean opt_write_settings = FALSE;
	static gboolean opt_display_settings = FALSE;
	static gboolean opt_erase_settings = FALSE;
        GError *error = NULL;
        GOptionContext *context = NULL;
        static GOptionEntry entries[] =
                {
                        { "verbose", 'v', 0, G_OPTION_ARG_NONE, &opt_verbose, "Verbose operation", NULL},
                        { "no-ui", 'n', 0, G_OPTION_ARG_NONE, &opt_noui, "Don't show any dialogs", NULL},
                        { "block", 'b', 0, G_OPTION_ARG_NONE, &opt_block, "Allow gnome-mount to block for UI", NULL},
                        { "unmount", 'u', 0, G_OPTION_ARG_NONE, &opt_unmount, "Unmount", NULL},
			{ "connect-crypto", 0, 0, G_OPTION_ARG_NONE, &opt_connect_crypto, "Setup crypto device", NULL},
			{ "disconnect-crypto", 0, 0, G_OPTION_ARG_NONE, &opt_disconnect_crypto, "Tear down crypto device", NULL},
                        { "eject", 'e', 0, G_OPTION_ARG_NONE, &opt_eject, "Eject", NULL},
                        { "hal-udi", 'h', 0, G_OPTION_ARG_STRING, &opt_hal_udi, "Mount by HAL UDI", NULL},
                        { "device", 'd', 0, G_OPTION_ARG_STRING, &opt_device_file, "Mount by device file", NULL},
                        { "pseudonym", 'p', 0, G_OPTION_ARG_STRING, &opt_nickname, "Mount by one of device's nicknames: mountpoint, label, with or without directory prefix", NULL},
			{ "text", 't', 0, G_OPTION_ARG_NONE, &opt_nodisplay, "Text-based operation", NULL},

			{ "mount-point", 'm', 0, G_OPTION_ARG_STRING, &opt_mount_point, "Specify mount point", NULL},
			{ "mount-options", 'o', 0, G_OPTION_ARG_STRING, &opt_mount_options, "Specify mount options", NULL},
			{ "extra-mount-options", '\0', 0, G_OPTION_ARG_STRING, &opt_extra_mount_options, "Specify extra mount options", NULL},
			{ "fstype", 'f', 0, G_OPTION_ARG_STRING, &opt_fstype, "Specify file system type", NULL},
			{ "write-settings", '\0', 0, G_OPTION_ARG_NONE, &opt_write_settings, "Don't mount; write given settings for volume/drive", NULL},
			{ "display-settings", '\0', 0, G_OPTION_ARG_NONE, &opt_display_settings, "Don't mount; display settings for volume/drive", NULL},
			{ "erase-settings", '\0', 0, G_OPTION_ARG_NONE, &opt_erase_settings, "Don't mount; erase settings for volume/drive", NULL},
                        { NULL, 0, 0, 0, NULL, NULL, NULL }
                };

	rc = 1;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (PACKAGE, "UTF-8");
	textdomain (PACKAGE);

        context = g_option_context_new ("- GNOME mount");
        g_option_context_add_main_entries (context, entries, PACKAGE);
        g_option_context_add_group (context, gtk_get_option_group (FALSE));
        g_option_context_parse (context, &argc, &argv, &error);

	mount_point_from_command_line = g_strdup (opt_mount_point);
	mount_options_from_command_line = g_strdup (opt_mount_options);
	extra_mount_options_from_command_line = g_strdup (opt_extra_mount_options);
	fstype_from_command_line = g_strdup (opt_fstype);

	if (opt_disconnect_crypto) {
		opt_unmount = TRUE;
	}

	/* these options are only meaningful from the commandline */
	if (opt_display_settings ||
	    opt_write_settings ||
	    opt_erase_settings) {
		opt_nodisplay = TRUE;
	}

        if (opt_verbose)
                g_print ("%s\n", PACKAGE_STRING);

	if (!gtk_init_check (&argc, &argv)) {
                if (opt_verbose)
                        g_print (_("X display not available - using text-based operation.\n"));
		opt_nodisplay = TRUE;
	}

	if (opt_nodisplay) {
		opt_block = TRUE;
		opt_noui = TRUE;
	}

	/* If we want to show dialogs, show that in a child process and let the parent exit immediately
	 * so we don't block
	 */
	if (!opt_block) {
		if (pipe (fds) != 0) {
			fprintf (stderr, "Cannot create pipe\n");
			goto error;
		}
		
		pid = fork();
		switch (pid) {
		case 0:
			/* child; do our real work and notify parent when we know the exit code */
			break;
		case -1:
			/* failure */
			fprintf (stderr, "Cannot fork\n");
			goto error;
			
		default:
			/* parent; wait on rc over pipe from child */
			close (fds[1]);
			read (fds[0], &buf, 1);
			if (buf == '1')
				rc = 0;
			goto error;
		}

		/* close unused descriptor */
		close (fds[0]);

		/* we have to complete daemonize, because otherwise the caller
		 * might wait for the to finish too (e.g.  processes using
		 * libgnomevfs such as Nautilus or any application using the
		 * gnomevfs backend of the filechooser).
		 */
		dev_null_fd = open ("/dev/null", O_RDWR);
		/* ignore if we can't open /dev/null */
		if (dev_null_fd >= 0) {
			/* attach /dev/null to stdout, stdin, stderr */
			dup2 (dev_null_fd, 0);
			dup2 (dev_null_fd, 1);
			dup2 (dev_null_fd, 2);
			close (dev_null_fd);
		}
		setsid ();
	}

	if (!opt_noui) {
		gtk_init (&argc, &argv);
#ifdef ENABLE_NOTIFY
		notify_init ("gnome-mount");
#endif
	}

	if (!opt_verbose) {
		g_log_set_handler (NULL, G_LOG_LEVEL_DEBUG, my_empty_log_handler, NULL);
	}

	gconf_client = gconf_client_get_default ();

	hal_ctx = do_hal_init ();
	if (hal_ctx == NULL)
		goto out;

	if (opt_device_file != NULL) {
#ifndef sun
		resolved_device_file = resolve_symlink (opt_device_file);
		if (resolved_device_file == NULL) {
			goto out;
		}
		if (strcmp (resolved_device_file, opt_device_file) != 0) {
			g_print (_("Resolved device file %s -> %s\n"), opt_device_file, resolved_device_file);
		}
#else /* sun */
		resolved_device_file = g_strdup (opt_device_file);
#endif /* sun */
	}
		
	if (opt_hal_udi != NULL) {
		volume = libhal_volume_from_udi (hal_ctx, opt_hal_udi);
	} else if (resolved_device_file != NULL) {
		volume = libhal_volume_from_device_file (hal_ctx, resolved_device_file);
	} else if (opt_nickname != NULL) {
		volume = volume_from_nickname (hal_ctx, opt_nickname);
		if (volume != NULL) {
			g_print (_("Resolved pseudonym \"%s\" -> %s\n"), opt_nickname, get_dev_file (volume, NULL));
		} else {
                        if (opt_verbose)
                                g_message (_("Cannot resolve pseudonym \"%s\" to a volume\n"), opt_nickname);
			goto out;
		}
	} else {
		g_print (_("Use --hal-udi, --device or --pseudonym to specify volume\n"));
		goto out;
	}

	if (volume == NULL) {
		if (opt_hal_udi != NULL) {
			drive = libhal_drive_from_udi (hal_ctx, opt_hal_udi);
		} else if (resolved_device_file != NULL) {
			drive = libhal_drive_from_device_file (hal_ctx, resolved_device_file);
		}

		if (drive != NULL) {
			char **ifs;
			gboolean found;

			if (opt_display_settings ||
			    opt_write_settings ||
			    opt_erase_settings) {
				/* it's perfectly allowed to tweak settings on drives that can detect media, e.g. 
				 * drives that don't have the org.fd.Hal.Device.Volume interface 
				 */
				goto try_drive;
			}


			found = FALSE;

			ifs = libhal_device_get_property_strlist (hal_ctx,
								  libhal_drive_get_udi (drive),
								  "info.interfaces",
								  NULL);
			if (ifs != NULL) {
				unsigned int i;

				for (i = 0; ifs[i] != NULL; i++) {
					if (strcmp (ifs[i], "org.freedesktop.Hal.Device.Volume") == 0) {
						found = TRUE;
						g_debug ("Will attempts methods on drive object");
						break;
					}
				}
				libhal_free_string_array (ifs);
				
			} 

			if (found) {
				goto try_drive;
			} else {
                                if (opt_eject) {
                                        char **volumes_udis;
                                        int num_volumes;
                                        int n;

                                        /* HACK TODO XXX ALERT: ugh.. try to find a mountable volume with an eject
                                         * method..
                                         */
                                        volumes_udis = libhal_drive_find_all_volumes (hal_ctx, drive, &num_volumes);
                                        if (volumes_udis != NULL) {
                                                for (n = 0; n < num_volumes; n++) {
                                                        char **vol_ifs;
                                                        int m;

                                                        vol_ifs = libhal_device_get_property_strlist (
                                                                hal_ctx,
                                                                volumes_udis[n],
                                                                "info.interfaces",
                                                                NULL);
                                                        if (vol_ifs != NULL) {
                                                                for (m = 0; vol_ifs[m] != NULL; m++) {
                                                                        if (strcmp (ifs[m], "org.freedesktop.Hal.Device.Volume") == 0) {
                                                                                volume = libhal_volume_from_udi (hal_ctx, volumes_udis[n]);
                                                                                libhal_drive_free (drive);
                                                                                drive = NULL;
                                                                                goto try_drive;
                                                                        }
                                                                }
                                                        }
                                                        libhal_free_string_array (vol_ifs);
                                                }
                                                libhal_free_string_array (volumes_udis);
                                        }

                                }


                                if (opt_verbose)
                                        g_message (_("Drive %s does not contain media."), 
                                                   resolved_device_file != NULL ? resolved_device_file : opt_hal_udi);
				
				notify_parent (FALSE);
				
				show_error_dialog_no_media (NULL, NULL, NULL);
			}

		} else {
			/* Silently fail */
                        if (opt_verbose)
                                g_message (_("Given device '%s' is not a volume or a drive."), 
                                           resolved_device_file != NULL ? resolved_device_file : opt_hal_udi);
		}

		goto out;
	}

try_drive:

	/* from here on either volume!=NULL or drive!=NULL */
	if (volume != NULL)
		udi = libhal_volume_get_udi (volume);
	else
		udi = libhal_drive_get_udi (drive);

	if (opt_display_settings) {
		display_settings (volume, drive);
		goto out;
	} else if (opt_write_settings) {
		write_settings (volume, drive);
		goto out;
	} else if (opt_erase_settings) {
		erase_settings (volume, drive);
		goto out;
	}


	/* infer from commandline */
	char *basename;

	basename  = g_path_get_basename (argv[0]);
	if (strcmp (basename, "gnome-umount") == 0) {
		opt_unmount = TRUE;
	} else if (strcmp (basename, "gnome-eject") == 0) {
		opt_eject = TRUE;
	}

	g_free (basename);

	if (opt_unmount && opt_eject) {
                if (opt_verbose)
                        g_message (_("Cannot unmount and eject simultaneously"));
		goto out;
	}

	if (opt_unmount) {
		const char *fstype;
		LibHalVolumeUsage fsusage;

		if (volume != NULL) {
			fstype = libhal_volume_get_fstype (volume);
			fsusage = libhal_volume_get_fsusage (volume);
		} else {
			fstype = "";
			fsusage = LIBHAL_VOLUME_USAGE_UNKNOWN;
		}

		if (fsusage == LIBHAL_VOLUME_USAGE_CRYPTO) {
			if (teardown_crypto (udi, volume, drive))
				rc = 0;
		} else {
			if (volume_unmount (udi, volume, drive)) {

				/* as a convenience also tear down the crypto backing volume if appropriate */
				if (volume != NULL) {
					const char *backing_udi;
					backing_udi = libhal_volume_crypto_get_backing_volume_udi (volume);
					if (backing_udi != NULL) {
						LibHalVolume *backing_volume;
						backing_volume = libhal_volume_from_udi (hal_ctx, backing_udi);
						if (backing_volume != NULL) {
							if (teardown_crypto (backing_udi, backing_volume, NULL))
								rc = 0;
							libhal_volume_free (backing_volume);
						}
					} else {
						rc = 0;
					}
				}
			}
		}

	} else if (opt_eject) {
                if (volume_eject (udi, volume, drive))
                        rc = 0;
	} else {
		LibHalVolumeUsage fsusage;

		if (volume != NULL) {
			fsusage = libhal_volume_get_fsusage (volume);
		} else {
			fsusage = LIBHAL_VOLUME_USAGE_UNKNOWN;
		}

		if (fsusage == LIBHAL_VOLUME_USAGE_CRYPTO) {
			char *password;
			gboolean password_error;
			gboolean setup_success;
			char *clear_udi;
			gboolean password_retry;
			int password_num_tries;

			/* see if we're already setup */
			clear_udi = libhal_volume_crypto_get_clear_volume_udi (hal_ctx, volume);
			if (clear_udi != NULL) {
                                if (opt_verbose)
                                        g_message (_("Crypto volume '%s' is already setup with clear volume '%s'"), 
                                                   udi, clear_udi);

				free (clear_udi);
				goto out;
			}

                        /* don't prompt for password if the device is locked
                         * (e.g. https://bugzilla.redhat.com/show_bug.cgi?id=437309 )
                         */
                        if (libhal_device_is_locked_by_others (hal_ctx,
                                                               libhal_volume_get_udi (volume),
                                                               "org.freedesktop.Hal.Device.Storage",
                                                               NULL)) {
                                if (opt_verbose)
                                        g_message (_("Crypto device %s is locked"),
                                                   libhal_volume_get_udi (volume));
                                goto out;
                        }

			g_debug ("Crypto volume - UDI '%s'", udi);

			setup_success = FALSE;

			/* we need this to catch when the cleartext device is added */
			crypto_setup_backing_udi = udi;
			libhal_ctx_set_device_added (hal_ctx, crypto_setup_device_added);
			/* we need to catch this if the backing device is removed (to remove the password dialog) */
			libhal_ctx_set_device_removed (hal_ctx, crypto_setup_device_removed);

			password_retry = FALSE;
			password_num_tries = 0;
		retry_password:			
			password = get_password (udi, volume, drive, password_retry);
			password_num_tries++;
			if (password != NULL) {
				setup_success = setup_crypto (udi, volume, drive, password, &password_error);
				if (!setup_success && password_error) {
                                        if (opt_verbose)
                                                g_message (_("Bad crypto password"));
					if (!opt_nodisplay && password_num_tries < 3) {
						g_free (password);
						password_retry = TRUE;
						goto retry_password;
					} else {
                                                if (opt_verbose)
                                                        g_message (_("Bailing out..."));
					}
				}
			}

			/* mount the clear volume except if --connect-crypto was explicitly passed */
			if (setup_success && !opt_connect_crypto) {
				
				/* wait as we try to mount the cleartext volume */
				g_debug ("Waiting for cleartext volume backed by %s..", udi);
				/* add a timeout as we don't want to wait forever */
				g_timeout_add (10 * 1000, crypto_setup_timeout, NULL);
				gtk_main();
				
				rc = crypto_setup_rc;
			}
			
			g_free (password);
			
			goto out;

		} else if (fsusage == LIBHAL_VOLUME_USAGE_MOUNTABLE_FILESYSTEM ||
			   fsusage == LIBHAL_VOLUME_USAGE_UNKNOWN) {
			if (volume_mount (udi, volume, drive))
				rc = 0;
		}

	}

	notify_parent (rc == 0);

out:	
 	if (drive != NULL)
		libhal_drive_free (drive);

 	if (volume != NULL)
		libhal_volume_free (volume);

	if (hal_ctx != NULL)
		libhal_ctx_free (hal_ctx);

	if (resolved_device_file != NULL)
		g_free (resolved_device_file);

error:
	if (context != NULL)
		g_option_context_free (context);

	return rc;
}
