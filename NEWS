==========
gnome-mount 0.8
==========

Released April 16, 2008

Changes from 0.7

- Get rid of libgnomeui dependency
- Don't show lock dialogs if underlying device is locked in HAL
- Mention partition number for the unlock dialog
- Make eject work on the main block device

Lots of new and updated translations (see po/ChangeLog)

==========
gnome-mount 0.7
==========

Released October 11, 2007

Changes from 0.6

- Support for PolicyKit if HAL supports PolicyKit
- Minor bugfixes

Lots of new and updated translations (see po/ChangeLog)

==========
gnome-mount 0.6
==========

Released April 10, 2007

Changes from 0.5

- default to shortname=lower for vfat (#390201)
- better handling of NTFS (#394947)
- get rid of libgnomeui dependency (Jani Monoses)
- FreeBSD support (Joe Marcus Clarke)

Lots of new translations (see po/ChangeLog)

==========
gnome-mount 0.5
==========

Released September 19, 2006

Changes from 0.4

 - general fixes
 - use mount(1) for volumes mentioned in /etc/fstab
 - fixes for Solaris (Artem Kachitchkine)
 - read mount point and settings from gconf
 - man page for the terminal obsessive
 - extension for Nautilus
 - notifications for volumes taking a long time to unmount 

Translations

 - en_GB: David Lodge
 - es: Francisco Javier F. Serrador
 - it: Luca Ferretti
 - ja: Takeshi AIHANA
 - mk: Jovan Naumovski
 - nb: Kjartan Maraas
 - ru: Leonid Kanter
 - sv: Daniel Nylander
 - vi: Nguyễn Thái Ngọc Duy
 - zh_CN: Funda Wang

Requirements for gnome-mount 0.5

 - GTK+                  >= 2.8
 - gnome-keyring         >= 0.4
 - libgnomeui            >= 2.2
 - gconf-2.0             >= 2.8
 - libglade-2.0          >= 2.8
 - HAL                   >= 0.5.8.1
 - DBUS                  >= 0.60  (with glib bindings)
 - libnotify             >= 0.3 (optional)
 - libnautilus-extension >= 2.5.0 (optional)

==========
gnome-mount 0.4
==========

Released February 24, 2006

Changes from 0.3

 - Provide gnome-mount pkg-config file
 - Don't block if putting up dialogs
 - Error dialogs
 - Allow pseudonyms for specifying device (Artem Kachitchkine)
 - Can now mount/unmount/eject drives that HAL cannot poll
 - Support for passworded media with gnome-keyring integration
 - Experimental Nautilus extension (disabled by default)
 - Resolve symlinks for --device
 - Works when X is not available (includes password prompt)
 - Lots of general clean ups

Requirements for gnome-mount 0.4

 - GTK+          >= 2.8
 - gnome-keyring >= 0.4
 - libgnomeui    >= 2.2
 - HAL           >= 0.5.7
 - DBUS          >= 0.60  (with glib bindings)
