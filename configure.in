AC_PREREQ(2.52)

AC_INIT(gnome-mount, 0.9)
AC_CONFIG_SRCDIR(src)
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)
AM_CONFIG_HEADER(config.h)

AC_PROG_CC
AC_PROG_LN_S
AM_PROG_LIBTOOL

changequote(,)dnl
if test "x$GCC" = "xyes"; then
  case " $CFLAGS " in
  *[\ \	]-W[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -W" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Waggregate-return[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Waggregate-return" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wmissing-prototypes[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wmissing-prototypes" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wmissing-declarations[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wmissing-declarations" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wcast-align[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wcast-align" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wbad-function-cast[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wbad-function-cast" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wdisabled-optimization[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wdisabled-optimization" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wpointer-arith[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wpointer-arith" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wredundant-decls[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wredundant-decls" ;;
  esac

  case " $CFLAGS " in
  *[\ \	]-Wcast-qual[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wcast-qual" ;;
  esac
fi
changequote([,])dnl


AM_GLIB_GNU_GETTEXT

GETTEXT_PACKAGE=AC_PACKAGE_NAME
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Gettext package.])

IT_PROG_INTLTOOL([0.35.0])

dnl Whether to build Nautilus extension
AC_MSG_CHECKING(whether to enable Nautilus extension)
AC_ARG_ENABLE(nautilus_extension, AS_HELP_STRING([--enable-nautilus-extension],[Build Nautilus extension]),,[enable_nautilus_extension=yes])
if test "x$enable_nautilus_extension" = "xyes"; then
        AC_DEFINE([ENABLE_NAUTILUS_EXTENSION],[],[Enable Nautilus Extension])
        AC_MSG_RESULT(yes)
	nautilus_extensiondir="`pkg-config --variable extensiondir libnautilus-extension`"
	if test "x$nautilus_extensiondir" = "x"; then
		nautilus_extensiondir="\$(libdir)/nautilus/extensions-2.0"
	fi
	AC_SUBST(nautilus_extensiondir)
else
        AC_MSG_RESULT(no)
fi
AM_CONDITIONAL(ENABLE_NAUTILUS_EXTENSION, test "x$enable_nautilus_extension" = "xyes")

GLIB_REQUIRED=2.15.0
NAUTILUS_REQUIRED=2.21.2

PKG_CHECK_MODULES(GNOME_MOUNT, gconf-2.0 gnome-keyring-1 >= 0.4 dbus-glib-1 >= 0.31 hal-storage >= 0.5.8.1 gtk+-2.0 >= 2.8.0)
AC_SUBST(GNOME_MOUNT_CFLAGS)
AC_SUBST(GNOME_MOUNT_LIBS)

if test "x$enable_nautilus_extension" = "xyes"; then
   PKG_CHECK_MODULES(GNOME_MOUNT_PROPERTIES, libglade-2.0 hal-storage >= 0.5.8.1 dbus-1 >= 0.60 dbus-glib-1 >= 0.60 gtk+-2.0 >= 2.8.0 glib-2.0 >= $GLIB_REQUIRED)
   AC_SUBST(GNOME_MOUNT_PROPERTIES_CFLAGS)
   AC_SUBST(GNOME_MOUNT_PROPERTIES_LIBS)

   PKG_CHECK_MODULES(NAUTILUS, libnautilus-extension >= $NAUTILUS_REQUIRED)
   AC_SUBST(NAUTILUS_CFLAGS)
   AC_SUBST(NAUTILUS_LIBS)
fi

PKG_CHECK_MODULES([NOTIFY], [libnotify >= 0.7.0], [enable_notify=yes], [enable_notify=no])
if test "x$enable_notify" != "xno"; then
   AC_DEFINE_UNQUOTED([ENABLE_NOTIFY], 1, [Enable notifications with libnotify])
   AC_SUBST(NOTIFY_CFLAGS)
   AC_SUBST(NOTIFY_LIBS)
fi

AC_PATH_PROG(GCONFTOOL, gconftool-2)
AM_GCONF_SOURCE_2

AC_OUTPUT([
Makefile
src/Makefile
src/copy-paste/Makefile
po/Makefile.in
gnome-mount.pc
gnome-mount.1
])

echo "
         gnome-mount $VERSION
         ===============

Build Nautilus extension:                $enable_nautilus_extension
Use libnotify:                           $enable_notify

        gnome-mount has been configured. 
"

