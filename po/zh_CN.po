# Chinese (China) translation for gnome-mount
# Copyright (c) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-mount package.
# Aron Xu <happyaron.xu@gmail.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-mount\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-09-01 14:51+0800\n"
"PO-Revision-Date: 2009-09-01 14:54+0800\n"
"Last-Translator: Aron Xu <happyaron.xu@gmail.com>\n"
"Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../gnome-mount.schemas.in.h:1
msgid ""
"A list of default mount options for volumes formatted with the iso9660 file "
"system."
msgstr "为 iso9660 文件系统卷的默认挂载选项列表。"

#: ../gnome-mount.schemas.in.h:2
msgid ""
"A list of default mount options for volumes formatted with the ntfs file "
"system using ntfs-3g."
msgstr "为 ntfs 文件系统卷并使用 ntfs-3g 的默认挂载选项列表"

#: ../gnome-mount.schemas.in.h:3
msgid ""
"A list of default mount options for volumes formatted with the ntfs file "
"system."
msgstr "为 ntfs 文件系统卷的默认挂载选项列表"

#: ../gnome-mount.schemas.in.h:4
msgid ""
"A list of default mount options for volumes formatted with the udf file "
"system."
msgstr "为 udf 文件系统卷的默认挂载选项列表"

#: ../gnome-mount.schemas.in.h:5
msgid ""
"A list of default mount options for volumes formatted with the vfat file "
"system."
msgstr "为 vfat 文件系统卷的默认挂载选项列表"

#: ../gnome-mount.schemas.in.h:6
msgid "Default mount options for iso9660 fs"
msgstr "iso9660 文件系统的默认挂载选项"

#: ../gnome-mount.schemas.in.h:7
msgid "Default mount options for ntfs fs"
msgstr "ntfs 文件系统的默认挂载选项"

#: ../gnome-mount.schemas.in.h:8
msgid "Default mount options for ntfs-3g fs"
msgstr "ntfs-3g 文件系统的默认挂载选项"

#: ../gnome-mount.schemas.in.h:9
msgid "Default mount options for udf fs"
msgstr "udf 文件系统的默认挂载选项"

#: ../gnome-mount.schemas.in.h:10
msgid "Default mount options for vfat fs"
msgstr "vfat 文件系统的默认挂载选项"

#: ../gnome-mount.schemas.in.h:11
msgid ""
"The name of the file system driver to use by default when 'ntfs' is "
"detected. This is useful for configuring the system to use the 'ntfs-3g' "
"driver for all 'ntfs' file systems by default. User can still choose to use "
"the 'ntfs' file system driver by overriding it per-volume or on the command "
"line."
msgstr ""
"当 \"ntfs\" 被检测到了使用默认的文件系统驱动名称。这对配置系统使用 \"ntfs-3g"
"\" 驱动对所有默认 \"ntfs\" 文件系统很有帮助。用户仍然可以选择使用 \"ntfs\"　"
"文件系统驱动通过卷或者命令行来撤销它。"

#: ../gnome-mount.schemas.in.h:12
msgid "When 'ntfs' is detected, what file system driver to use by default"
msgstr "当“ntfs”格式被侦测到，将会使用默认的格式载入"

#: ../src/copy-paste/gnome-password-dialog.c:210
msgid "_Username:"
msgstr "用户名(_U)："

#: ../src/copy-paste/gnome-password-dialog.c:213
msgid "_Domain:"
msgstr "域(_D)："

#: ../src/copy-paste/gnome-password-dialog.c:216
msgid "_Password:"
msgstr "密码(_P)："

#: ../src/copy-paste/gnome-password-dialog.c:219
msgid "_New password:"
msgstr "新密码(_N)："

#: ../src/copy-paste/gnome-password-dialog.c:220
msgid "Con_firm password:"
msgstr "确认密码(_F)："

#: ../src/copy-paste/gnome-password-dialog.c:223
msgid "Password quality:"
msgstr "密码强度："

#: ../src/copy-paste/gnome-password-dialog.c:338
msgid "Co_nnect"
msgstr "连接(N)"

#: ../src/copy-paste/gnome-password-dialog.c:371
msgid "Connect _anonymously"
msgstr "匿名连接(_A)"

#: ../src/copy-paste/gnome-password-dialog.c:376
msgid "Connect as u_ser:"
msgstr "连接身份(_S)："

#: ../src/copy-paste/gnome-password-dialog.c:438
msgid "_Forget password immediately"
msgstr "不记住密码(_F)"

#: ../src/copy-paste/gnome-password-dialog.c:444
msgid "_Remember password until you logout"
msgstr "在注销前记住密码(_R)"

#: ../src/copy-paste/gnome-password-dialog.c:450
msgid "_Remember forever"
msgstr "永久记住(_R)"

#: ../src/gnome-mount.c:190
msgid "Unable to mount media."
msgstr "无法挂载介质"

#: ../src/gnome-mount.c:193
msgid "There is probably no media in the drive."
msgstr "可能驱动器中没有介质。"

#: ../src/gnome-mount.c:223
msgid "Cannot eject volume"
msgstr "无法弹出卷"

#: ../src/gnome-mount.c:234
#, c-format
msgid "You are not privileged to eject the volume '%s'."
msgstr "您不允许弹出卷 '%s'。"

#: ../src/gnome-mount.c:235
msgid "You are not privileged to eject this volume."
msgstr "您不允许弹出这个卷。"

#: ../src/gnome-mount.c:243
#, c-format
msgid "An application is preventing the volume '%s' from being ejected."
msgstr "某个应用程序正在阻止 '%s' 的弹出。"

#: ../src/gnome-mount.c:244
msgid "An application is preventing the volume from being ejected."
msgstr "某个应用程序正在阻止弹出卷。"

#: ../src/gnome-mount.c:253
#, c-format
msgid ""
"There was an error ejecting the volume or drive.\n"
"\n"
"<b>%s:</b> %s"
msgstr ""
"在弹出卷或者驱动时出现了一个错误。\n"
"\n"
"<b>%s：</b> %s"

#: ../src/gnome-mount.c:300
msgid "Cannot unmount volume"
msgstr "无法卸下卷"

#: ../src/gnome-mount.c:311
#, c-format
msgid "You are not privileged to unmount the volume '%s'."
msgstr "无法卸下卷“%s”。"

#: ../src/gnome-mount.c:312
msgid "You are not privileged to unmount this volume."
msgstr "您不允许卸下这个卷。"

#: ../src/gnome-mount.c:321
#, c-format
msgid "An application is preventing the volume '%s' from being unmounted."
msgstr "某个应用程序正在阻止卸下“%s”。"

#: ../src/gnome-mount.c:322
msgid "An application is preventing the volume from being unmounted."
msgstr "某个应用程序正在使用该卷以至于不能立刻卸载该卷。"

#: ../src/gnome-mount.c:329
#, c-format
msgid "The volume '%s' is not mounted."
msgstr "卷“%s”未挂载。"

#: ../src/gnome-mount.c:330
msgid "The volume is not mounted."
msgstr "卷未挂载。"

#: ../src/gnome-mount.c:338
#, c-format
msgid "Cannot unmount the volume '%s'."
msgstr "无法卸载卷“%s”。"

#: ../src/gnome-mount.c:339
msgid "Cannot unmount the volume."
msgstr "无法卸载卷"

#: ../src/gnome-mount.c:347
#, c-format
msgid "The volume '%s' was probably mounted manually on the command line."
msgstr "卷“%s”可能是在命令行手动挂载的。"

#: ../src/gnome-mount.c:348
msgid "The volume was probably mounted manually on the command line."
msgstr "该卷可能是在命令行手动挂载的。"

#: ../src/gnome-mount.c:358 ../src/gnome-mount.c:512
#, c-format
msgid "Error <i>%s</i>."
msgstr "错误 <i>%s</i>."

#: ../src/gnome-mount.c:368 ../src/gnome-mount.c:524
msgid "_Details"
msgstr "详细信息(_D)"

#: ../src/gnome-mount.c:421
msgid "Cannot mount volume."
msgstr "无法挂载卷。"

#: ../src/gnome-mount.c:432
#, c-format
msgid "You are not privileged to mount the volume '%s'."
msgstr "您没有挂载卷“%s”的权限。"

#: ../src/gnome-mount.c:433
msgid "You are not privileged to mount this volume."
msgstr "您没有挂载这个卷的权限。"

#: ../src/gnome-mount.c:440
#, c-format
msgid "Invalid mount option when attempting to mount the volume '%s'."
msgstr "尝试挂载卷“%s”时遇到非法挂载选项。"

#: ../src/gnome-mount.c:441
msgid "Invalid mount option when attempting to mount the volume."
msgstr "尝试挂载卷时遇到非法挂载选项。"

#: ../src/gnome-mount.c:447
#, c-format
msgid ""
"The volume '%s' uses the <i>%s</i> file system which is not supported by "
"your system."
msgstr "卷“%s”使用了 <i>%s</i> 文件系统，但您的系统不支持它。"

#: ../src/gnome-mount.c:448
#, c-format
msgid ""
"The volume uses the <i>%s</i> file system which is not supported by your "
"system."
msgstr "该卷使用了 <i>%s</i> 文件系统，但您的系统不支持它。"

#: ../src/gnome-mount.c:464
#, c-format
msgid "Unable to mount the volume '%s'."
msgstr "无法挂载卷“%s”。"

#: ../src/gnome-mount.c:465
msgid "Unable to mount the volume."
msgstr "无法挂载卷。"

#: ../src/gnome-mount.c:480
#, c-format
msgid "Volume '%s' is already mounted."
msgstr "卷“%s”已经挂载。"

#: ../src/gnome-mount.c:481
msgid "Volume is already mounted."
msgstr "卷已经挂载。"

#: ../src/gnome-mount.c:851
#, c-format
msgid "Mounted %s at \"%s\"\n"
msgstr "挂载 %s 到 \"%s\"\n"

#: ../src/gnome-mount.c:1181 ../src/gnome-mount.c:1844
#: ../src/gnome-mount.c:2080
#, c-format
msgid "Device %s is locked, aborting"
msgstr "锁设备 %s 被锁定，中止"

#: ../src/gnome-mount.c:1187 ../src/gnome-mount.c:1849
#: ../src/gnome-mount.c:2086
#, c-format
msgid "Device %s is in /etc/fstab with mount point \"%s\"\n"
msgstr "设备 %s 处于 /etc/fstab 中，挂载点是“%s”\n"

#: ../src/gnome-mount.c:1232
#, c-format
msgid "Mounted %s at \"%s\" (using /etc/fstab)\n"
msgstr "已挂载 %s 到“%s” (使用 /etc/fstab)\n"

#: ../src/gnome-mount.c:1594
msgid "Writing data to device"
msgstr "正在向设备中写入数据"

#: ../src/gnome-mount.c:1595
#, c-format
msgid ""
"There is data that needs to be written to the device %s before it can be "
"removed. Please do not remove the media or disconnect the drive."
msgstr ""
"在设备 %s 可被移除前仍有数据需要写入到其中。请不要移除该驱动器。"

#: ../src/gnome-mount.c:1645
msgid "Device is now safe to remove"
msgstr "现在可以安全地拔下设备了。"

#: ../src/gnome-mount.c:1646
#, c-format
msgid "The device %s is now safe to remove."
msgstr "现在可以安全地拔下设备 %s 了。"

#: ../src/gnome-mount.c:1887
#, c-format
msgid "Unmounted %s (using /etc/fstab)\n"
msgstr "已卸下 %s (使用 /etc/fstab)\n"

#: ../src/gnome-mount.c:1969
#, c-format
msgid "Unmounted %s\n"
msgstr "已卸下 %s\n"

#: ../src/gnome-mount.c:2124
#, c-format
msgid "Ejected %s (using /etc/fstab).\n"
msgstr "已弹出 %s (使用 /etc/fstab)。\n"

#: ../src/gnome-mount.c:2205
#, c-format
msgid "Ejected %s\n"
msgstr "已弹出 %s\n"

#: ../src/gnome-mount.c:2347
#, c-format
msgid "Enter password to unlock encrypted data for %s: "
msgstr "输入密码以解锁加密的数据 %s: "

#: ../src/gnome-mount.c:2371
#, c-format
msgid ""
"The storage device %s contains encrypted data on partition %d. Enter a "
"password to unlock."
msgstr "存储设备 %s 包含了加密的数据在分区 %d。输入密码来解锁。"

#: ../src/gnome-mount.c:2375
#, c-format
msgid "Unlock Encrypted Data (partition %d)"
msgstr "解锁加密的数据 (分区 %d)"

#: ../src/gnome-mount.c:2378
#, c-format
msgid ""
"The storage device %s contains encrypted data. Enter a password to unlock."
msgstr "此存储器 %s 里包含加密资料。请输入密码来解锁。"

#: ../src/gnome-mount.c:2381
#, c-format
msgid "Unlock Encrypted Data"
msgstr "解锁加密的数据"

#: ../src/gnome-mount.c:2486
#, c-format
msgid "Setup clear-text device for %s.\n"
msgstr "为 %s 设置明文设备。\n"

#: ../src/gnome-mount.c:2553
#, c-format
msgid "Teared down clear-text device for %s.\n"
msgstr "为 %s 卸下明文设备。\n"

#: ../src/gnome-mount.c:2600
#, c-format
msgid "Clear text device is %s. Mounting.\n"
msgstr "明文设备为 %s，正在挂载。\n"

#: ../src/gnome-mount.c:2939
#, c-format
msgid "X display not available - using text-based operation.\n"
msgstr "X 显示不可用 - 使用基于字符界面的操作。\n"

#: ../src/gnome-mount.c:3020
#, c-format
msgid "Resolved device file %s -> %s\n"
msgstr "解析设备文件 %s -> %s\n"

#: ../src/gnome-mount.c:3034
#, c-format
msgid "Resolved pseudonym \"%s\" -> %s\n"
msgstr "解析别名 %s -> %s\n"

#: ../src/gnome-mount.c:3037
#, c-format
msgid "Cannot resolve pseudonym \"%s\" to a volume\n"
msgstr "无法将别名 \"%s\" 解析为卷。\n"

#: ../src/gnome-mount.c:3041
#, c-format
msgid "Use --hal-udi, --device or --pseudonym to specify volume\n"
msgstr "使用 --hal-udi, --device 或 --pseudonym 来指定卷\n"

#: ../src/gnome-mount.c:3127
#, c-format
msgid "Drive %s does not contain media."
msgstr "驱动器 %s 为空。"

#: ../src/gnome-mount.c:3138
#, c-format
msgid "Given device '%s' is not a volume or a drive."
msgstr "给出的设备'%s'不是卷和驱动器"

#: ../src/gnome-mount.c:3179
msgid "Cannot unmount and eject simultaneously"
msgstr "无法同时进行卸载和弹出"

#: ../src/gnome-mount.c:3244
#, c-format
msgid "Crypto volume '%s' is already setup with clear volume '%s'"
msgstr "锁卷 '%s' 已经使用干净的卷　'%s'　配置了"

#: ../src/gnome-mount.c:3259
#, c-format
msgid "Crypto device %s is locked"
msgstr "锁设备 %s 被锁定"

#: ../src/gnome-mount.c:3283
msgid "Bad crypto password"
msgstr "错误的加密密码"

#: ../src/gnome-mount.c:3290
msgid "Bailing out..."
msgstr "排出..."

#: ../src/gnome-mount-properties.c:169 ../src/gnome-mount-properties.c:197
msgid "Volume"
msgstr "卷"

#: ../src/gnome-mount-properties.c:210
msgid "Drive"
msgstr "驱动"

#: ../src/gnome-mount-properties.glade.h:1
msgid "<b>Connection:</b>"
msgstr "<b>连接：</b>"

#: ../src/gnome-mount-properties.glade.h:2
msgid "<b>External:</b>"
msgstr "<b>外部：</b>"

#: ../src/gnome-mount-properties.glade.h:3
msgid "<b>File System:</b>"
msgstr "<b>文件系统：</b>"

#: ../src/gnome-mount-properties.glade.h:4
msgid "<b>Firmware:</b>"
msgstr "<b>固件：</b>"

#: ../src/gnome-mount-properties.glade.h:5
msgid "<b>Label:</b>"
msgstr "<b>标签：</b>"

#: ../src/gnome-mount-properties.glade.h:6
msgid "<b>Media:</b>"
msgstr "<b>介质：</b>"

#: ../src/gnome-mount-properties.glade.h:7
msgid "<b>Model:</b>"
msgstr "<b>型号：</b>"

#: ../src/gnome-mount-properties.glade.h:8
msgid "<b>Mount Options:</b>"
msgstr "<b>挂载选项：</b>"

#: ../src/gnome-mount-properties.glade.h:9
msgid "<b>Mount Point:</b>"
msgstr "<b>挂载点：</b>"

#: ../src/gnome-mount-properties.glade.h:10
msgid "<b>Removable:</b>"
msgstr "<b>可移除：</b>"

#: ../src/gnome-mount-properties.glade.h:11
msgid "<b>Serial:</b>"
msgstr "<b>序列：</b>"

#: ../src/gnome-mount-properties.glade.h:12
msgid "<b>Settings</b>"
msgstr "<b>设置</b>"

#: ../src/gnome-mount-properties.glade.h:13
msgid "<b>Size:</b>"
msgstr "<b>大小：</b>"

#: ../src/gnome-mount-properties.glade.h:14
msgid "<b>UUID:</b>"
msgstr "<b>UUID：</b>"

#: ../src/gnome-mount-properties.glade.h:15
msgid "<b>Vendor:</b>"
msgstr "<b>供应商：</b>"

#: ../src/gnome-mount-properties.glade.h:16
msgid ""
"<small><i>Changes in settings will not take effect until the\n"
"volume is remounted.</i></small>"
msgstr ""
"<small><i>设置的变更在卷重新挂载之后\n"
"才会生效。</i></small>"

#: ../src/gnome-mount-properties.glade.h:18
msgid ""
"<small><i>Settings for a drive affects all volumes inserted in\n"
"the drive. This can be overriden for individual \n"
" volumes using the \"Volume\" tab.</i></small>"
msgstr ""
"<small><i>对驱动器的设置影响所有插入该驱动器的卷。\n"
"使用 \"Volume\" 标签对单独卷进行设置可以覆盖它。</i></small>"

#: ../src/gnome-mount-properties-view.c:571
#: ../src/gnome-mount-properties-view.c:923
msgid "Removable Hard Disk"
msgstr "移动硬盘"

#: ../src/gnome-mount-properties-view.c:574
#: ../src/gnome-mount-properties-view.c:926
msgid "Hard Disk"
msgstr "硬盘"

#: ../src/gnome-mount-properties-view.c:581
msgid "CD-ROM Disc"
msgstr "CD-ROM 盘片"

#: ../src/gnome-mount-properties-view.c:585
msgid "Blank CD-R Disc"
msgstr "空 CD-R 盘片"

#: ../src/gnome-mount-properties-view.c:587
msgid "CD-R Disc"
msgstr "CD-R 盘片"

#: ../src/gnome-mount-properties-view.c:591
msgid "Blank CD-RW Disc"
msgstr "空 CD-RW 盘片"

#: ../src/gnome-mount-properties-view.c:593
msgid "CD-RW Disc"
msgstr "CD-RW 盘片"

#: ../src/gnome-mount-properties-view.c:596
msgid "DVD-ROM Disc"
msgstr "DVD-ROM 光盘"

#: ../src/gnome-mount-properties-view.c:600
msgid "Blank DVD-RAM Disc"
msgstr "空 DVD-RAM 盘片"

#: ../src/gnome-mount-properties-view.c:602
msgid "DVD-RAM Disc"
msgstr "DVD-RAM 盘片"

#: ../src/gnome-mount-properties-view.c:606
msgid "Blank DVD-R Disc"
msgstr "空 DVD-R 盘片"

#: ../src/gnome-mount-properties-view.c:608
msgid "DVD-R Disc"
msgstr "DVD-R 盘片"

#: ../src/gnome-mount-properties-view.c:612
msgid "Blank DVD-RW Disc"
msgstr "空 DVD-RW 盘片"

#: ../src/gnome-mount-properties-view.c:614
msgid "DVD-RW Disc"
msgstr "DVD-RW 盘片"

#: ../src/gnome-mount-properties-view.c:618
msgid "Blank DVD+R Disc"
msgstr "空 DVD+R 盘片"

#: ../src/gnome-mount-properties-view.c:620
msgid "DVD+R Disc"
msgstr "DVD+R 盘片"

#: ../src/gnome-mount-properties-view.c:624
msgid "Blank DVD+RW Disc"
msgstr "空 DVD+RW 盘片"

#: ../src/gnome-mount-properties-view.c:626
msgid "DVD+RW Disc"
msgstr "DVD+RW 盘片"

#: ../src/gnome-mount-properties-view.c:630
msgid "Blank DVD+R Dual Layer Disc"
msgstr "空 DVD+R 双层盘片"

#: ../src/gnome-mount-properties-view.c:632
msgid "DVD+R Dual Layer Disc"
msgstr "DVD+R 双层盘片"

#: ../src/gnome-mount-properties-view.c:636
msgid "Blank BD-R Disc"
msgstr "空 BD-R 盘片"

#: ../src/gnome-mount-properties-view.c:638
msgid "BD-R Disc"
msgstr "BD-R 盘片"

#: ../src/gnome-mount-properties-view.c:642
msgid "Blank BD-RE Disc"
msgstr "空 BD-RE 盘片"

#: ../src/gnome-mount-properties-view.c:644
msgid "BD-RE Disc"
msgstr "BD-RE 盘片"

#: ../src/gnome-mount-properties-view.c:647
msgid "HD DVD-ROM Disc"
msgstr "HD DVD-ROM 盘片"

#: ../src/gnome-mount-properties-view.c:651
msgid "Blank HD DVD-R Disc"
msgstr "空 HD DVD-ROM 盘片"

#: ../src/gnome-mount-properties-view.c:653
msgid "HD DVD-R Disc"
msgstr "HD DVD-R 盘片"

#: ../src/gnome-mount-properties-view.c:657
msgid "Blank HD DVD-RW Disc"
msgstr "空高清DVD-RW 盘片"

#: ../src/gnome-mount-properties-view.c:659
msgid "HD DVD-RW Disc"
msgstr "高清DVD-RW 盘片"

#: ../src/gnome-mount-properties-view.c:665
msgid "Audio Disc"
msgstr "音频 CD"

#: ../src/gnome-mount-properties-view.c:671
msgid "Floppy Disk"
msgstr "软盘"

#: ../src/gnome-mount-properties-view.c:674
msgid "Tape"
msgstr "磁带"

#: ../src/gnome-mount-properties-view.c:677
#: ../src/gnome-mount-properties-view.c:977
msgid "CompactFlash "
msgstr "压缩闪存 "

#: ../src/gnome-mount-properties-view.c:680
#: ../src/gnome-mount-properties-view.c:980
msgid "MemoryStick"
msgstr "记忆棒"

#: ../src/gnome-mount-properties-view.c:683
#: ../src/gnome-mount-properties-view.c:983
msgid "SmartMedia"
msgstr "智能介质"

#: ../src/gnome-mount-properties-view.c:686
#: ../src/gnome-mount-properties-view.c:986
msgid "SecureDigital / MultiMediaCard"
msgstr "SD/MMC 存储卡"

#: ../src/gnome-mount-properties-view.c:689
#: ../src/gnome-mount-properties-view.c:989
msgid "Digital Camera"
msgstr "数码相机"

#: ../src/gnome-mount-properties-view.c:692
#: ../src/gnome-mount-properties-view.c:992
msgid "Digital Audio Player"
msgstr "数字音频播放器"

#: ../src/gnome-mount-properties-view.c:695
#: ../src/gnome-mount-properties-view.c:995
msgid "Zip"
msgstr "Zip"

#: ../src/gnome-mount-properties-view.c:698
#: ../src/gnome-mount-properties-view.c:998
msgid "Jaz"
msgstr "Jaz"

#: ../src/gnome-mount-properties-view.c:701
#: ../src/gnome-mount-properties-view.c:1001
msgid "Flash Drive"
msgstr "闪存"

#: ../src/gnome-mount-properties-view.c:704
#: ../src/gnome-mount-properties-view.c:1004
msgid "Unknown Media"
msgstr "未知介质"

#: ../src/gnome-mount-properties-view.c:768
#: ../src/gnome-mount-properties-view.c:775
msgid "<i>Unknown</i>"
msgstr "<i>未知</i>"

#: ../src/gnome-mount-properties-view.c:780
#: ../src/gnome-mount-properties-view.c:781
#: ../src/gnome-mount-properties-view.c:782
msgid "<i>Not Mounted</i>"
msgstr "<i>未挂载</i>"

#: ../src/gnome-mount-properties-view.c:848
msgid "ATA"
msgstr "ATA"

#: ../src/gnome-mount-properties-view.c:851
msgid "SCSI"
msgstr "SCSI"

#: ../src/gnome-mount-properties-view.c:857
msgid "USB"
msgstr "USB"

#: ../src/gnome-mount-properties-view.c:910
msgid "Firewire/IEEE1394"
msgstr "火线/IEEE 1394"

#: ../src/gnome-mount-properties-view.c:913
msgid "CCW"
msgstr "CCW"

#: ../src/gnome-mount-properties-view.c:917
msgid "Unknown Connection"
msgstr "未知连接"

#: ../src/gnome-mount-properties-view.c:936
msgid "CD-ROM"
msgstr "CD-ROM"

#: ../src/gnome-mount-properties-view.c:938
msgid "CD-R"
msgstr "CD-R"

#: ../src/gnome-mount-properties-view.c:940
msgid "CD-RW"
msgstr "CD-RW"

#: ../src/gnome-mount-properties-view.c:944
msgid "DVD-ROM"
msgstr "DVD-ROM"

#: ../src/gnome-mount-properties-view.c:946
msgid "DVD+R"
msgstr "DVD+R"

#: ../src/gnome-mount-properties-view.c:948
msgid "DVD+RW"
msgstr "DVD+RW"

#: ../src/gnome-mount-properties-view.c:950
msgid "DVD-R"
msgstr "DVD-R"

#: ../src/gnome-mount-properties-view.c:952
msgid "DVD-RW"
msgstr "DVD-RW"

#: ../src/gnome-mount-properties-view.c:954
msgid "DVD-RAM"
msgstr "DVD-RAM"

#: ../src/gnome-mount-properties-view.c:957
msgid "DVD±R"
msgstr "DVD±R"

#: ../src/gnome-mount-properties-view.c:960
msgid "DVD±RW"
msgstr "DVD±RW"

#: ../src/gnome-mount-properties-view.c:963
#, c-format
msgid "%s/%s Drive"
msgstr "%s/%s 驱动器"

#: ../src/gnome-mount-properties-view.c:965
#, c-format
msgid "%s Drive"
msgstr "%s 驱动器"

#: ../src/gnome-mount-properties-view.c:971
msgid "Floppy Drive"
msgstr "软盘驱动器"

#: ../src/gnome-mount-properties-view.c:974
msgid "Tape Drive"
msgstr "磁带驱动器"

#: ../src/gnome-mount-properties-view.c:1010
msgid "Yes (ejectable)"
msgstr "是（可弹出）"

#: ../src/gnome-mount-properties-view.c:1012
#: ../src/gnome-mount-properties-view.c:1019
msgid "Yes"
msgstr "是"

#: ../src/gnome-mount-properties-view.c:1015
#: ../src/gnome-mount-properties-view.c:1021
msgid "No"
msgstr "否"

#~ msgid ""
#~ "A list of default mount options for volumes formatted with the hfs file "
#~ "system."
#~ msgstr "为 hfs 文件系统卷的默认挂载选项列表"

#~ msgid "Default mount options for hfs fs"
#~ msgstr "hfs 文件系统的默认挂载选项"

#~ msgid "Unable to set up crypto device"
#~ msgstr "无法设置加密设备"

#~ msgid "You need to install the cryptsetup package."
#~ msgstr "您需要安装 cryptsetup 软件包。"

#~ msgid ""
#~ "Error <i>%s</i>\n"
#~ "\n"
#~ "%s"
#~ msgstr ""
#~ "错误 <i>%s</i>\n"
#~ "\n"
#~ "%s"

#~ msgid ""
#~ "To prevent data loss, wait until this has finished before disconnecting."
#~ msgstr "为避免数据丢失，请在断开连接前等待此过程结束。"
