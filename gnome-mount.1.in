.\" 
.\" gnome-mount manual page.
.\" Copyright (C) 2006 Red Hat, Inc.
.\"
.TH gnome-mount 1
.SH NAME
gnome-mount \- Mount drives and volumes using
.B HAL
and read settings from the GNOME desktop configuration system
.B gconf.
.SH SYNOPSIS
.PP
.B gnome-mount
[-?|--help] [-v] [-n] [-t] [-b]
[-d /dev/file | -h /org/fd/Hal/udi | -p nickname]
[--unmount | --eject | --write-settings | --erase-settings | --show-settings]
[--mount-point where-to-mount]
[--mount-options opt1,opt2=foo,opt3]
[--extra-mount-options opt4,opt5=bar]
[--fstype fstype-to-use]

.SH DESCRIPTION

This program is used to mount and unmount file systems for GNOME
desktop users. It can also be used to eject discs from CD drives and
other devices that needs to be ejected. For example, iPod's needs this
to make the "Do not disconnect" message go away.

Normally, this program is invoked by software in the GNOME stack
(specifically
.B gnome-vfs-daemon
and
.B gnome-volume-manager
). End users should never have to deal with gnome-mount directly
on the command line, nor should they have to read this manual 
page.

Mounting a file system into the root file system involves a certain
degree of configuration and as such is subject to whatever preferences
an user might have. 
.B gnome-mount 
allows the user to control the mount point location, the mount options
and what file system to use for mounting a file system. The settings
are read from the 
.B gconf 
database (which is per-user) and can also be overridden on the command
line using the appropriate parameters. See below.

.SH SPECIFYING THE TARGET

The target (e.g. the partition or file system to mount, unmount or
eject) can be specified using the
.B HAL
UDI (Unique Device Identifier), e.g.
.B /org/freedesktop/Hal/devices/volume_uuid_E18B_10EC
, the name of the special device file, e.g.
.B /dev/sda1
or a pseudonym. 
The latter is a textual string used to locate the target and it makes
.B gnome-mount
search for the target by comparing the given textual string to the
mount points and file system labels.

.SH DETERMINING SETTINGS

Settings (e.g. mount point, mount options, file system type) are read
in the order below. Note that each option is treated individually; for
example it is valid for a drive to only specify the mount point
setting and not the mount options. Also note that the even if the
drive specifies mount options, these can be overridden on a per-volume
basis.

.TP
.I FILE SYSTEM DEFAULTS
First, default mount options are read from
.B /system/storage/default_options/FS_TYPE
for the probed file system type of the volume. The option
.B uid=, 
is treated specially by
.B gnome-mount
and will be replaced by
.B uid=UID_OF_USER
to cope with the fact that the
.B uid
is a function of the user calling it.

.TP
.I PER DRIVE
Second, the gconf tree at 
.B /system/storage/drives/UDI_OF_DRIVE
is consulted for options that depend on what drive the volume belongs
to. For example, this is useful for configuring that volumes inserted
into a given drive is always mounted at the same location. For example,
this can be used to emulate
.B /etc/fstab
behaviour by where CD media is always mounted at e.g.
.B /media/cdrom

.TP
.I PER VOLUME
Third, the gconf tree at 
.B /system/storage/volumes/UDI_OF_VOLUME
is consulted for options that are specific to a particular piece of
media and as such depends on either the file system label (e.g.
.B EOS_DIGITAL
) or the file system UUID (e.g.
.B E18B_10EC
) or both.

.TP
.I COMMANDLINE OPTIONS
Users can pass
.B --mount-point
, 
.B --mount-options
or
.B --fstype
on the commandline to override settings.

.TP
.I EXTRA COMMANDLINE OPTIONS
Finally, if mount options are passed via
.B --extra-mount-options
these are not replacing the mount options, they are simply added.
This is useful for doing e.g. 

 gnome-mount --extra-mount-options remount,exec -d /dev/sda1

to remount a volumes such that programs can be run from the media.
This is useful for e.g. gnome-volume-manager if it discovers an
autorun file on the media.

.SH PASSWORDED MEDIA

.B gnome-mount
supports passworded media through the 
.I org.freedesktop.Hal.Device.Volume.Crypto
interface supported by
.B HAL
and this includes volumes formatted in a way that adheres to the
.B LUKS
(Linux Unified Key Setup) specification. In addition, 
.B gnome-mount
uses
.B gnome-keyring
to retrieve and store the pass phrase. If no key can be retrieved,
.B gnome-mount
will prompt the user for one. In addition, if the keyring is
locked, the user may be prompted to unlock it via standard
.B gnome-keyring
mechanisms.

.SH PRIVILEGES

.B gnome-mount
is intended for unprivileged users and
.B HAL
ultimately controls if the calling user is allowed to mount, unmount
or eject volumes as well as what mount options are valid. As such,
requests may be denied. See the (human readable) exception returned 
from
.B HAL 
for details if a request fails.

Note that
.B HAL
has a notion of what mount options are valid for a given volume. They
are listed in the HAL property 
.I volume.mount.valid_options
on the device object representing the volume to mount. Consult
.BR lshal (1)
for details. Also note that HAL by default appends the options
.B nosuid
and 
.B nodev
to prevent privilege escalation.

In addition to using
.B HAL
as the mechanism for mounting file systems, the
.B /etc/fstab
file is also consulted as
.B HAL
will refuse to mount any file system listed in this file as it would
violate system policy. If this is the case,
.B gnome-mount
will invoke
.BR mount (1)
as the calling user rather than invoking the
.B Mount
method on the
.I org.freedesktop.Hal.Device.Volume
interface on the device object representing the volume / drive. This
means that settings (mount point, mount options, file system type)
read by
.B gnome-mount
are not passed along as these are already specified in the
.B /etc/fstab
file and there are no mechanism to override them. When parsing the
.B /etc/fstab
file, 
.B gnome-mount
(and also
.B HAL
for that matter)
resolves symbolic links and also respects the
.B LABEL=
and
.B UUID=
notations. For example, if this line is in
.B /etc/fstab

 LABEL=MyVolume /mnt/myvolume auto user,defaults 0 0

then 
.B gnome-mount
mounts the file system with the label MyVolume via
.BR mount (1)
and
.B /etc/fstab
rather than using the
.B HAL
mechanisms.

.SH OPTIONS

Options available for the
.B gnome-mount
command:

.TP
.B -v, --verbose
Verbose operation, shows debug messages.

.TP
.B -n, --no-ui
Don't show any dialogs the user needs to dismiss. If X11 is
available,
.B gnome-mount
may pop up transient notification bubbles e.g. suggesting the user to
remount a volume with different options to streamline access to file
systems with ownership attributes. This is the option that storage
policy daemons such as
.B gnome-volume-manager
should invoke
.B gnome-mount
in. File managers, however, such as 
.B Nautilus
, should never use this option as the user should get e.g. an error
dialog if he tries to access a volume with a missing, unsupported or
unknown file system.

.TP
.B -b, --block
Allow 
.B gnome-mount 
to block even if an error occured. By default,
.B gnome-mount
will daemonize so it can return control to the invoking application as
soon as possible (e.g. when an operation either fails or succeeds )
while still showing an error dialog to the end user. Useful when
debugging.

.TP
.B -u, --unmount
Use this for unmounting rather than mounting. If
.B gnome-mount
is invoked as 
.B gnome-umount
(a symlink to
.B gnome-mount
) then this option is automatically selected.

.TP
.B -e, --eject
Use this for ejecting rather than mounting. If
.B gnome-mount
is invoked as
.B gnome-eject
(a symlink to
.B gnome-mount
) then this option is automatically selected.


.TP
.B -d, --device
Specify target volume by the special device file.

.TP
.B -h, --hal-udi
Specify target volume by
.B HAL
UDI (Unique Device Identifier).

.TP
.B -p, --pseudonym
Specify target volume by pseudonym. See above for how this works.

.TP
.B -t, --text
Never use X11 dialogs or notification bubbles even if an X11 server is
available. Also prohibits the use of
.B gnome-keyring
to retrieve pass phrases for passworded media because this might
require unlocking the keyring which happens through an X11 dialog
out of process. Useful for command line operation.

.TP
.B -m, --mount-point
Specify mount point to use; don't include
.B /media
as this is automatically appened by the mechanism used to mount,
e.g. the
.B HAL
methods.

.TP
.B -o, --mount-options
Specify mount options. Separate by comma.

.TP
.B -f, --fstype
Specify file system type. This is useful for using e.g. the
.B msdos
file system instead of the
.B vfat
file system.

.TP
.B --write-settings
Instead of mounting a drive, specify what options to store in the
gconf database. Can be used on both drives and volumes. Be careful
using this with the
.B --device
option as optical drives (among others) use the same special device
file for both the drive and the volume. One trick is to ensure the
optical drive has no media when configuring it via this option. 
Another possibility is to use the
.B HAL
UDI instead.

.TP
.B --display-settings
Display settings for a drive or volume.

.TP
.B --erase-settings
Erase settings for a drive or volume.

.SH RETURN CODES

.B gnome-mount 
will return zero if the request succeeded or non-zero if it
failed. Note that
.B gnome-mount
is specifically designed to run in a graphical user environment and as
such all error reporting (and resolution) is through X11 dialogs. For
example, if
.B HAL
reports that a volume could not be mounted because of a missing
file system driver, 
.B gnome-mount
might, one day, launch a tool to ask the user if he wants to download
and install the driver. In a similar way, all error dialogs are
presented via X11 dialogs to the user as well.

.SH EXAMPLE

We want to make sure that the discs inserted into an optical drive
are always mounted at
.B /media/cdrecorder
instead of using the default which is using the label specified in the
.B iso9660
or 
.B udf
file system header. Assuming that the drive is empty and the special
device file for the drive is
.B /dev/hdc
the following command will work

    gnome-mount --write-settings
                --device /dev/hdc
                --mount-point cdrecorder

This can be inspected via the
.B --display-settings
option and the settings can also be erased via the
.B --erase-settings
option. Also note that
.BR gconf-editor (1)
can be used for tasks like these.

.SH HARDWARE THAT CANNOT BE POLLED

.B HAL
polls most storage devices for media insertion / removal and maintains
the list of devices exported. However, some hardware cannot be polled
for media changes without making noise or for other reasons. PC floppy
drives, Zip drives connected through an IDE interface and broken
optical drives falls into this category.

For such hardware
.B HAL
only exports the drive and rather than exporting volume as childs of
the drive, the
.I org.freedesktop.Hal.Device.Volume
interface is exported on the drive itself. 
.B gnome-mount
supports this but it means that it is impossible to know ahead of time
what file system is on the media in the problematic drive, so in this
case
.B gnome-mount
passes
.B auto
as the file system type and passes the mount options
.B uid=UID_OF_USER
as most media in such devices are formatted with either the 
.B vfat
, 
.B udf
or 
.B iso9660
file systems.

This also means that per-volume settings are
not possible; one can only specify settings per-drive.

.SH AUTHOR

.B gnome-mount
was written by David Zeuthen <david@fubar.dk>.

.SH SEE ALSO

.BR fstab (5),
.BR mount (8),
.BR umount (8),
.BR eject (1),
.BR cryptsetup (8),
.BR gconftool-2 (1),
.BR gconf-editor (1),
.BR lshal (1)

.B http://www.gnome.org/projects/gconf/

.B http://en.wikipedia.org/wiki/GNOME_Keyring

.B http://freedesktop.org/Software/hal

.B http://luks.endorphin.org


